package eu.k5.hashcode.q2016

fun main(args: Array<String>) {
    var total = 0L
    total += solve("busy_day") { SolverWithDroneAssignement(it, 2.49, true, true, busyDay()) }
    total += solve("mother_of_all_warehouses") { SolverByDistance(it, 2.5) }
    total += solve("redundancy") { SolverWithDroneAssignement(it, 2.51, true, true, ExclusiveItems(false, 20, 170)) }
    println("total " + total)
}

fun busyDay(): ExclusiveItems {
    return ExclusiveItems(false, 5, 170)
}

fun solve(name: String, solver: (Problem) -> Solver): Long {
    val problem = Problem.parse(name)
   // analyseProduct(problem)
    //74278 warehouse SolverByDIstance

    val solver = solver(problem)
    val solution = solver.solve()

    val score = solution.score()
    println(name + " " + score)

    return score
//    checkForDoubleItem(problem)
}


fun analyseProduct(problem: Problem) {
    val sorted = ArrayList(problem.products).sorted()
    for ((product, size) in problem.products.withIndex()) {
        var count = 0
        for (order in problem.orders) {
            if (order.items.contains(product)) {
                count += 1
            }
        }
        if (25 >= size && size * count > 190) {
            println("total $size $count $product")

            for (warehous in problem.warehouses) {
                if (warehous.products[product] * size > 190) {
                    val c = warehous.products[product]
                    println("w $size $c $product " + warehous.id
                    )

                }
            }
        }
    }
}

fun countProducts(problem: Problem) {
    for (order in problem.orders) {

    }
}