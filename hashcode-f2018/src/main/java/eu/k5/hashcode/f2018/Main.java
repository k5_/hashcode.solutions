package eu.k5.hashcode.f2018;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

	public static void main(String[] args) {

		handle("a_example");
		handle("b_short_walk");
		handle("c_going_green");
		handle("d_wide_selection");
		handle("e_precise_fit");
		handle("f_different_footprints");

	}

	public static void handle(String problemName) {

		Problem problem = read(problemName);

		Solution solution = solve(problem);

		print(problemName, solution);
	}

	public static Solution solve(Problem problem) {

		SolverOneResidential solver = new SolverOneResidential(problem);
		Solution solution = solver.solve();

		return solution;

	}

	public static Problem read(String problemName) {

		try (BufferedReader reader = Files.newBufferedReader(Paths.get("in", problemName + ".in"))) {

			Problem problem = ProblemParser.parse(reader);
			return problem;
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public static void print(String problemName, Solution solution) {
		if (solution == null) {
			return;
		}
		try (BufferedWriter writer = Files.newBufferedWriter(Paths.get("target", problemName + ".out"))) {

			solution.write(writer);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}
}
