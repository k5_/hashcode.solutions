package eu.k5.hashcode.q2016

import java.util.function.BiFunction

class SolverByDistance(val problem: Problem, val weightWeight: Double) : Solver {

    private var openOrders: ArrayList<DeliverOrder> = ArrayList()

    private val drones = ArrayList<Drone>()
    private val solution = Solution(problem)


    init {

        for (order in problem.orders) {
            openOrders.add(DeliverOrder(order.id, order.location, HashSet<Int>(order.items), order.weight))
        }

        openOrders.sortWith(ByDistanceComparator(problem.warehouses[0].location, weightWeight))

        for (drone in 0 until problem.drones) {
            drones.add(Drone(drone, 0, problem.warehouses[0].location))
        }

    }

    override fun solve(): Solution {

        //  ordersByDistance.removeAll { it.weight > problem.capacity }

        while (!openOrders.isEmpty()) {
            for (drone in 0 until problem.drones) {
                val use = ArrayList<DeliverPart>()
                val current = HashSet<Int>()
                var weight = 0
                var checked = 0

                drones[drone].location = problem.warehouses[0].location

                while (checked <= 3) {
                    openOrders = ArrayList(openOrders.sortedWith(ByDistanceComparator(drones[drone].location, weightWeight)))

                    for (order in openOrders) {
                        if (current.contains(order.id)) {
                            continue
                        }
                        current.add(order.id)
                        var added = false
                        for (item in order.openItems) {
                            val productWeight = problem.products[item]
                            if (weight + productWeight < problem.capacity) {
                                use.add(DeliverPart(order, item))
                                weight += productWeight
                                drones[drone].location = order.location
                                added = true
                            } else {
                                break
                            }
                        }

                        if (added) {
                            break
                        } else {
                            checked++
                        }
                    }
                    checked++
                }
                val completed = doOrders(drone, 0, use)
//            executed.addAll(use.map { it.id })
                openOrders.removeAll(completed)
            }
        }

        return solution
    }

    private fun doOrders(drone: Int, warehouse: Int, parts: List<DeliverPart>): ArrayList<DeliverOrder> {
        //   println(parts.size)
        val required = HashMap<Int, Int>()
        for (part in parts) {
            required.compute(part.item, BiFunction { _, c -> (c ?: 0).plus(1) })
        }

        for (load in required) {
            solution.load(drone, warehouse, load.key, load.value)
        }

        val completed = ArrayList<DeliverOrder>()
        for (part in parts) {
            solution.addCommand(drone, Solution.Action.DELIVER, part.order.id, part.item, 1)
            part.order.openItems.remove(part.item)
            if (part.order.openItems.isEmpty()) {
                completed.add(part.order)
            }
        }
        return completed
    }
}


class ByDistancePartComparator(val location: Location) : Comparator<DeliverPart> {

    override fun compare(o1: DeliverPart?, o2: DeliverPart?): Int {
        val d1 = o1!!.order.location.distance(location)
        val d2 = o2!!.order.location.distance(location)


        return Integer.compare(d1.toInt(), d2.toInt())
    }
}

class ByDistanceComparator(val location: Location, val weight: Double) : Comparator<DeliverOrder> {

    override fun compare(o1: DeliverOrder?, o2: DeliverOrder?): Int {
        val d1 = o1!!.location.distance(location) + o1.weight / weight
        val d2 = o2!!.location.distance(location) + o2.weight / weight


        return Integer.compare(d1.toInt(), d2.toInt())
    }
}

class Drone(
        val id: Int,
        var warehouse: Int,
        var location: Location,
        var step: Int = -1) {

    val use = ArrayList<DeliverPart>()


    var weight = 0

    fun load(location: Location, weight: Int) {
        val distance = location.distance(this.location)
        step += distance + 1
        this.weight += weight
        this.location = location
    }

    fun deliver(location: Location) {
        val distance = location.distance(this.location)
        step += distance + 1
        this.location = location
    }

    fun reset() {
        use.clear()
        weight = 0
    }
}

class DeliverOrder(
        val id: Int,
        val location: Location,
        val openItems: HashSet<Int>,
        val weight: Int

)

class DeliverPart(
        val order: DeliverOrder,
        val item: Int
)