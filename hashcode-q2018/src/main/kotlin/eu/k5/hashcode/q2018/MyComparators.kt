package eu.k5.hashcode.q2018

class MyComparators {

    class ByStep : Comparator<Vehicle> {
        override fun compare(o1: Vehicle?, o2: Vehicle?): Int {
            return Integer.compare(o1!!.step, o2!!.step)
        }

    }


    class By3DDistance(
            private val location: Location,
            private val step: Int
    ) : Comparator<Problem.Ride> {

        override fun compare(o1: Problem.Ride?, o2: Problem.Ride?): Int {
            var d1 = o1!!.start.distance(location)
            var d2 = o2!!.start.distance(location)

            if (o1.startStep > d1 + step) {
                d1 = o1.startStep - step
            } else if (d1 + step > o1.latestStart) {
                d1 = Integer.MAX_VALUE
            }
            if (o2.startStep > d2 + step) {
                d2 = o2.startStep - step
            } else if (d2 + step > o2.latestStart) {
                d2 = Integer.MAX_VALUE
            }
            return Integer.compare(d1, d2)
        }
    }

    class By3DDistanceWithBonus(
            private val location: Location,
            private val step: Int,
            private val bonus: Int
    ) : Comparator<Problem.Ride> {

        override fun compare(o1: Problem.Ride?, o2: Problem.Ride?): Int {
            var d1 = o1!!.start.distance(location)
            var d2 = o2!!.start.distance(location)

            if (o1.startStep >= d1 + step) {
                d1 = o1.startStep - step - bonus
            } else if (d1 + step > o1.latestStart) {
                d1 = Integer.MAX_VALUE
            }
            if (o2.startStep >= d2 + step) {
                d2 = o2.startStep - step - bonus
            } else if (d2 + step > o2.latestStart) {
                d2 = Integer.MAX_VALUE
            }
            return Integer.compare(d1, d2)
        }
    }


}