package eu.k5.hashcode.q2017;

import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.stream.Collectors;

public class SolverGreedyByVideo implements Solver {

    private final Problem problem;
    private final Params params;
    private List<MainByRequest.VideoEndpoint> videoEndpoints;


    public SolverGreedyByVideo(Problem problem, Params params) {
        this.problem = problem;
        this.params = params;
    }

    private List<MainByRequest.VideoEndpoint> getVideoEndpoints() {
        if (videoEndpoints != null) {
            return videoEndpoints;
        }
        Map<MainByRequest.VideoEndpoint, List<Problem.Request>> videoEndpoints = new HashMap<>();
        for (Problem.Request request : problem.getRequests()) {
            MainByRequest.VideoEndpoint videoEndpoint = new MainByRequest.VideoEndpoint(request.getVideo(), request.getEndpoint());
            videoEndpoints.computeIfAbsent(videoEndpoint, (__) -> new ArrayList<>()).add(request);
        }
        this.videoEndpoints = new ArrayList<>();
        for (Map.Entry<MainByRequest.VideoEndpoint, List<Problem.Request>> videoEndpoint : videoEndpoints.entrySet()) {

            this.videoEndpoints.add(videoEndpoint.getKey().withRequests(videoEndpoint.getValue()));
        }
        return this.videoEndpoints;
    }

    @Override
    public Solution solve() {
        Solution solution = new Solution(problem, "byvideo");

        Map<Integer, VideoCachePotential> potential = createVideoPotential();
        while (true) {

            Optional<VideoCachePotential> max = findHighestPotential(potential);
            if (max.isPresent()) {
                VideoCachePotential pot = max.get();
                if (pot.isEmpty()) {
                    potential.remove(pot.video);
                    continue;
                }

                if (params.getVerbosity() != 0) {
                    System.out.println(potential.size() + " " + pot.getMax() + " " + pot.getMaxCache() + " " + pot.videoSize + " " + pot.caches.size());
                }
                Solution.UsedCache used = solution.getCache(pot.getMaxCache());

                if (used.getRemainingSize() >= pot.getVideoSize()) {
                    used.addVideo(pot.video, pot.videoSize);

                    checkFull(pot.getMaxCache(), used.getRemainingSize(), potential);


                    updatePotential(pot, solution);
                } else {
                    checkFull(pot.getMaxCache(), used.getRemainingSize(), potential);
                }
                if (pot.isEmpty()) {
                    potential.remove(pot.video);
                }
            } else {
                return solution;
            }
        }
    }

    private void checkFull(int cache, int remainingSize, Map<Integer, VideoCachePotential> potential) {
        AtomicInteger count = new AtomicInteger();
        potential.values().parallelStream().filter(p -> p.getMaxCache() == cache)
                .filter(p -> p.getVideoSize() > remainingSize)
                .forEach(pot -> {
                    pot.removeCache(cache);
                    count.incrementAndGet();
                });

        //System.out.println("Removed " + count.get());
    }


    private void updatePotential(VideoCachePotential potential, Solution solution) {
        potential.caches.clear();

        for (MainByRequest.VideoEndpoint videoEndpoint : potential.getEndpoints()) {
            Problem.Endpoint endpoint = problem.getEndpoints().get(videoEndpoint.getEndpoint());
            int bestLatency = endpoint.getLatency();

            for (Map.Entry<Integer, Integer> cacheEntry : endpoint.getCaches().entrySet()) {
                Solution.UsedCache used = solution.getCache(cacheEntry.getKey());
                if (used.contains(videoEndpoint.getVideo())) {
                    bestLatency = Math.min(cacheEntry.getValue(), bestLatency);
                }
                if (used.getRemainingSize() < potential.videoSize) {
                    potential.removeCache(cacheEntry.getKey());
                }
            }
            bestLatency = endpoint.getLatency() - bestLatency;

            for (Map.Entry<Integer, Integer> cacheEntry : endpoint.getCaches().entrySet()) {

                int cacheLatency = endpoint.getLatency() - cacheEntry.getValue() - bestLatency;
                if (cacheLatency > 0) {
                    potential.addCache(videoEndpoint.getTotalRequests(), cacheEntry.getKey(), cacheLatency);
                }
            }


        }
    }

    private Optional<VideoCachePotential> findHighestPotential(Map<Integer, VideoCachePotential> potential) {
        return potential.values().stream().max(new VideoPotentialComparator(params.getSizeWeight()));

    }


    private Map<Integer, VideoCachePotential> createVideoPotential() {

        Map<Integer, VideoCachePotential> potentials = new HashMap<>();

        for (MainByRequest.VideoEndpoint videoEndpoint : getVideoEndpoints()) {
            Problem.Endpoint endpoint = problem.getEndpoints().get(videoEndpoint.getEndpoint());

            VideoCachePotential potential = potentials.computeIfAbsent(videoEndpoint.getVideo(), VideoCachePotential::new);
            potential.videoSize = videoEndpoint.getVideoSize();
            potential.addVideo(videoEndpoint);
            for (Map.Entry<Integer, Integer> cacheEntry : endpoint.getCaches().entrySet()) {
                Integer latency = endpoint.getLatency() - cacheEntry.getValue();
                potential.addCache(videoEndpoint.getTotalRequests(), cacheEntry.getKey(), latency);
            }
        }

        return potentials;
    }


    static class VideoCachePotential {
        private final int video;
        private final Map<Integer, Long> caches = new HashMap<>();

        private int videoSize = Integer.MAX_VALUE;
        private long max;
        private int maxCache = -1;

        private List<MainByRequest.VideoEndpoint> endpoints = new ArrayList<>();
        private final Set<Integer> removed = new HashSet<>();

        public VideoCachePotential(int video) {
            this.video = video;
        }

        public void removeCache(int cache) {
            removed.add(cache);
            caches.remove(cache);
            max = -1l;
            maxCache = -1;
            for (Map.Entry<Integer, Long> entry : caches.entrySet()) {
                if (entry.getValue() > max) {
                    max = entry.getValue();
                    maxCache = entry.getKey();
                }
            }

        }

        public boolean isEmpty() {
            return caches.isEmpty();
        }

        public List<MainByRequest.VideoEndpoint> getEndpoints() {
            return endpoints;
        }

        public long getMax() {
            return max;
        }

        public void addVideo(MainByRequest.VideoEndpoint endpoint) {
            this.endpoints.add(endpoint);
        }

        public void addCache(int requests, Integer cache, Integer latency) {
            if (removed.contains(cache)) {
                return;
            }
            Long current = caches.getOrDefault(cache, 0l);
            current += requests * latency;
            if (current > max) {
                max = current;
                maxCache = cache;
            }
            caches.put(cache, current);
        }

        public int getMaxCache() {
            return maxCache;
        }

        public int getVideoSize() {
            return videoSize;
        }
    }

    static class VideoPotentialComparator implements Comparator<VideoCachePotential> {
        double sizeWeight;

        VideoPotentialComparator(double sizeWeight) {
            this.sizeWeight = sizeWeight;
        }

        @Override
        public int compare(VideoCachePotential o1, VideoCachePotential o2) {
            long m1 = o1.getMax() - (long) (o1.getVideoSize() * sizeWeight);
            long m2 = o2.getMax() - (long) (o2.getVideoSize() * sizeWeight);
            return Long.compare(m1, m2);
        }
    }
}