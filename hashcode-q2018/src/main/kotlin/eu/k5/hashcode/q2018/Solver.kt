package eu.k5.hashcode.q2018

interface Solver {


    fun solve() : Solution
}