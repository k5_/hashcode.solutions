package eu.k5.hashcode.f2018;

import static org.junit.Assert.*;

import org.junit.Test;

public class ProblemParserTest {

	@Test
	public void test() {

		Problem ex = Main.read("a_example");

		assertEquals(4, ex.getRows());
		assertEquals(7, ex.getCols());
		assertEquals(2, ex.getWalking());
		assertEquals(3, ex.getBuilding());
		assertEquals(3, ex.getBuildings().size());

	}

}
