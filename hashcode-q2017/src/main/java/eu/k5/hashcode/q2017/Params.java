package eu.k5.hashcode.q2017;

public class Params {

    private final boolean writeSolution;

    private final int dpRecalc;
    private final int verbosity;


    private double sizeWeight = 100.0;

    public Params(ParamsBuilder builder) {
        this.writeSolution = builder.writeSolution;
        this.dpRecalc = builder.dpRecalc;
        this.verbosity = builder.verbosity;
        this.sizeWeight = builder.sizeWeight;
    }

    public double getSizeWeight() {
        return sizeWeight;
    }

    public int getVerbosity() {
        return verbosity;
    }

    public boolean isWriteSolution() {
        return writeSolution;
    }

    public int getDpRecalc() {
        return dpRecalc;
    }

    public static ParamsBuilder builder() {
        return new ParamsBuilder();
    }

    public static class ParamsBuilder {
        private boolean writeSolution = false;

        private int dpRecalc = 1;

        private int verbosity = 0;

        private double sizeWeight = 64000.0;

        public ParamsBuilder writeSolution(boolean writeSolution) {
            this.writeSolution = writeSolution;
            return this;
        }

        public ParamsBuilder setVerbosity(int verbosity) {
            this.verbosity = verbosity;
            return this;
        }

        public ParamsBuilder setSizeWeight(double sizeWeight) {
            this.sizeWeight = sizeWeight;
            return this;
        }

        public Params build() {
            return new Params(this);
        }
    }
}
