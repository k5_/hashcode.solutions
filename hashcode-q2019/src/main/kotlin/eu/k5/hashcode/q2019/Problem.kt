package eu.k5.hashcode.q2019

import java.io.BufferedReader
import java.lang.reflect.Constructor
import java.nio.file.Files
import java.nio.file.Paths
import kotlin.collections.ArrayList
import kotlin.collections.HashMap

class Problem(
        val slideCount: Int


) {
    val photos = ArrayList<Photo>()

    val photosByTag = HashMap<Int, MutableList<Photo>>()

    class Photo(
            val photoId: Int,
            val horizontal: Boolean,
            val tags: IntArray
    )


    class Slide(
            val photo1: Photo,
            val photo2: Photo?,
            val tags: IntArray

    ) {
        constructor(photo: Photo) : this(photo, null, photo.tags)

        constructor(photo1: Photo, photo2: Photo) : this(photo1, photo2, Algorithms.join(photo1.tags, photo2.tags))
    }


    companion object {
        fun parse(name: String): Problem {
            return Files.newBufferedReader(Paths.get("in", "$name.txt")).use {
                parse(name, it!!)
            }
        }

        private fun parse(name: String, reader: BufferedReader): Problem {

            val header = reader.readLine();
            val problem = Problem(Integer.parseInt(header))

            val tagsIndex = HashMap<String, Int>();
            for (photoId in 0 until problem.slideCount) {
                val photoParts = reader.readLine().split(" ")

                val tags = ArrayList<Int>()
                for (index in 2 until photoParts.size) {
                    val tag = photoParts[index]
                    val tagIndex = tagsIndex.computeIfAbsent(tag) { tagsIndex.size }
                    tags.add(tagIndex)

                }
                tags.sort()
                val photo = Photo(photoId, photoParts[0] == "H", tags.toIntArray())

                problem.photos.add(photo)
                for (tag in photo.tags) {
                    problem.photosByTag.computeIfAbsent(tag) { ArrayList<Photo>() }.add(photo)
                }
            }
            return problem
        }
    }
}