package eu.k5.hashcode.q2019

import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.IntStream

class CombinerUngarian(
        val problem: Problem

) {

    val slides = ArrayList<Problem.Slide>()

    val combined = ArrayList<Problem.Slide>()

    fun combine(): List<Problem.Slide> {
        println("Combine")
        val photos = problem.photos.filter { !it.horizontal }
        for (photo in photos) {
            slides.add(Problem.Slide(photo))
        }

        if (photos.isEmpty()) {
            return combined
        }
        val sum = photos.sumBy { it.tags.size } / photos.size

        val matrix = initMatrix(sum)
        println("combine matrix: " + matrix.size)
        optimize(matrix)

        addToSolution(matrix)

        scoreCombine(sum)

        return combined


    }

    private fun scoreCombine(avg: Int) {
        var commonWaste = 0L
        var avgWaste = 0L
        for (slide in combined) {
            commonWaste += Algorithms.common(slide.photo1.tags, slide.photo2!!.tags)
            avgWaste += Algorithms.combineWaste(slide.photo1, slide.photo2!!, avg)
        }
        println("Common waste: $commonWaste avg waste: $avgWaste")
    }

    private fun optimize(matrix: Matrix): Int {
        var used = 0
        var lastCol = -1
        while (used < matrix.size) {

            matrix.subMinCols()
            matrix.subMinRows()
            matrix.refreshZeros()
            var added = 0
            while (true) {
                val colCell = matrix.zerosInCol.withIndex().filter { it.value > 0 }.minBy { it.value }
                if (colCell != null) {
                    val rowIndex = matrix.getZeroByMaxSum(colCell.index)
                    if (rowIndex != -1) {
                        matrix.markUsed(colCell.index, rowIndex)
                        lastCol = colCell.index
                        used++
                        //added++
                        matrix.zerosInRow[rowIndex] = 0
                    }
                    matrix.zerosInCol[colCell.index] = 0

                } else {
                    break
                }
                if (added > matrix.size / 20) {
                    println("added" + added + "used" + used)
                    matrix.refreshZeros()
                    added = 0
                }
            }
            //println("" + matrix.size + " " + used)
        }
        return lastCol
    }

    private fun addToSolution(matrix: Matrix) {


        for (col in 0 until matrix.size) {
            val first = matrix.photoMap[col]!!
            val row = matrix.usedByCol[col]
            val second = matrix.photoMapRows[row]!!

            combined.add(Problem.Slide(first.photo1, second.photo1))
        }

    }


    fun initMatrix(avg: Int): Matrix {
        println(avg)
        val photosSorted = slides//.sortedBy { it.tags.size }

        val matrix = Matrix(slides.size / 2)

        for (index in 0 until matrix.size) {
            matrix.photoMap[index] = photosSorted[index]
            matrix.photoMapRows[index] = photosSorted[index + matrix.size]
        }


        var max = AtomicInteger()
        IntStream.range(0, matrix.size).parallel().forEach {
            val index1 = it
            val photo1 = matrix.photoMap[index1]!!

            for (index2 in index1 + 1 until matrix.size) {
                val photo2 = matrix.photoMap[index2]!!

                val score = Algorithms.combineWaste(photo1, photo2, avg)
                matrix.set(index1, index2, score.toByte())
                matrix.set(index2, index1, score.toByte())

                var lmax = max.get()
                if (score > lmax) {

                    println("lmax" + lmax)
                    while (!max.compareAndSet(lmax, score)) {
                        lmax = max.get()
                        println("cas")
                        if (score <= lmax) {
                            break
                        }
                    }
                }
            }
        }
        return matrix
    }

}