package eu.k5.hashcode.q2018

import java.nio.file.Files
import java.nio.file.Path

class Solution(
        val problem: Problem
) {

    val vehicles = ArrayList<VehicleRides>()

    val usedRides = HashSet<Int>()

    init {
        for (v in 0..problem.vehicles) {
            vehicles.add(VehicleRides(v))
        }
    }

    fun addRide(vehicle: Int, ride: Int) {
        if (usedRides.contains(ride)) {
            throw IllegalArgumentException("Ride already used")
        }
        vehicles[vehicle].rides.add(ride)
        usedRides.add(ride)
    }


    fun score(): Long {
        val judge = Judge(problem, this)
        return judge.score()
    }

    fun write(path: Path) {
        Files.createDirectories(path)
        val file = path.resolve("${problem.name}.out")


        Files.newBufferedWriter(file).use {

            for (v in vehicles) {
                it.write(Integer.toString(v.rides.size))
                for(ride in v.rides){
                    it.write( " " + ride)
                }
                it.write("\n")
            }
        }

    }


    class VehicleRides(val id: Int) {
        val rides = ArrayList<Int>()
    }
}
