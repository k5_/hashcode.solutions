package eu.k5.hashcode.q2017;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.stream.Collectors;

import eu.k5.hashcode.q2017.Problem.Request;

public class MainByVideo {
    private static ProblemParser parser = new ProblemParser();

    public static void main(String[] args) throws IOException {

        // solve("example", 1);
        solve("me_at_the_zoo", Params.builder().build());
        // solve("trending_today");
        solve("videos_worth_spreading", Params.builder().build());
        // solve("kittens", 1);

    }

    public static long solve(String name, Params params) throws IOException {
        Problem problem = parser.parse(name);
        Solution solve = solve(problem, params);

        if (params.isWriteSolution()) {
            try (Writer writer = Files.newBufferedWriter(Paths.get(name + ".out"))) {
                solve.write(writer);
            }
        }
        return solve.judge();
    }

    private static Solution solve(Problem problem, Params params) {

        SolverDynamicProgramming solver = new SolverDynamicProgramming(problem, params);

        return solver.solve();
    }

    // private static class VideoPotential {
    // private final int video;
    // private final int videoSize;
    // private final List<Request> requests;
    // private long potential;
    //
    // public VideoPotential(int video, List<Request> requests) {
    // this.video = video;
    // this.videoSize = requests.get(0).getVideoSize();
    // this.requests = requests;
    // potential = 0;
    // }
    //
    // public void calcPotential(Problem problem) {
    // long totalRequests = 0;
    // for(Request request:requests){
    // totalRequests += request.getRequests();
    // }
    //
    //
    //
    // }
    //
    // }
    //
    // private static void createVideoPotentials(Problem problem) {
    // Map<Integer, List<Request>> byVideo = problem.getRequests().stream()
    // .collect(Collectors.groupingBy(Request::getVideo));
    //
    // for (Entry<Integer, List<Request>> video : byVideo.entrySet()) {
    //
    // VideoPotential potential = new VideoPotential(video.getKey(),
    // video.getValue());
    //
    // potential.calcPotential(problem);
    // }
    // }
}
