package eu.k5.hashcode.q2019

import java.lang.IllegalArgumentException
import java.util.stream.IntStream

class Matrix(
        val size: Int
) {
    private val itemsX: MutableList<ByteArray> = ArrayList<ByteArray>()


    val zerosInRow = IntArray(size)
    val zerosInCol = IntArray(size)

    private val sumRows = IntArray(size)
    private val sumCols = IntArray(size)

    private val rows = ArrayList<SolverUngarian.State>(size)
    private val cols = ArrayList<SolverUngarian.State>(size)

    val usedByCol = IntArray(size)
    val usedByRow = IntArray(size)

    val photoMap = HashMap<Int, Problem.Slide>()
    val photoMapRows = HashMap<Int, Problem.Slide>()

    companion object {
        val ITEM_CHUNK = 2_000_000_000
    }

    init {
        var totalItems: Long = size.toLong() * size.toLong()
        if (totalItems > ITEM_CHUNK) {
            itemsX.add(ByteArray(ITEM_CHUNK))
            totalItems -= ITEM_CHUNK
        } else {
            itemsX.add(ByteArray(totalItems.toInt()))
            totalItems = 0L
        }
        if (totalItems > ITEM_CHUNK) {
            itemsX.add(ByteArray(ITEM_CHUNK))
            totalItems -= ITEM_CHUNK
        } else {
            itemsX.add(ByteArray(totalItems.toInt()))
            totalItems = 0L
        }
        if (totalItems > ITEM_CHUNK) {
            itemsX.add(ByteArray(ITEM_CHUNK))
            totalItems -= ITEM_CHUNK
        } else {
            itemsX.add(ByteArray(totalItems.toInt()))
            totalItems = 0L
        }

        if (totalItems > ITEM_CHUNK) {
            throw IllegalArgumentException("Not enough space")
        } else {
            itemsX.add(ByteArray(totalItems.toInt()))
        }

        for (index in 0 until size) {
            rows.add(SolverUngarian.State.INIT)
            cols.add(SolverUngarian.State.INIT)
            usedByCol[index] = -1
            usedByRow[index] = -1
        }
    }

    fun set(col: Int, row: Int, value: Byte) {
        var calcIndex = calcIndex(col, row)
        val itemIndex = (calcIndex / ITEM_CHUNK).toInt()
        val index = (calcIndex % ITEM_CHUNK).toInt()
        itemsX[itemIndex][index] = value
    }

    fun get(col: Int, row: Int): Byte {
        var calcIndex = calcIndex(col, row)
        val itemIndex = (calcIndex / ITEM_CHUNK).toInt()
        val index = (calcIndex % ITEM_CHUNK).toInt()
        return itemsX[itemIndex][index]
    }

    inline fun calcIndex(col: Int, row: Int): Long {
        return col.toLong() + row.toLong() * size
    }

    inline fun calcCol(index: Long): Int {
        return (index % size).toInt()
    }

    inline fun calcRow(index: Long): Int {
        return (index / size).toInt()
    }

    fun diff(max: Int) {
        for (item in itemsX) {
            for (index in 0 until item.size) {
                item[index] = (max - item[index]).toByte()
            }
        }
    }

    private fun getRowMin(row: Int): Int {
        var min = get(0, row).toInt()
        var sum = 0
        for (index in 1 until size) {
            val value = get(index, row).toInt()
            sum += value
            min = Math.min(min, value)
        }
        sumRows[row] = sum
        return min
    }

    fun subMinRows() {

        for (row in 0 until size) {
            val rowMin = getRowMin(row)
            var used = 0;
            for (col in 0 until size) {
                val calcIndex = calcIndex(col, row)
                val itemIndex = (calcIndex / ITEM_CHUNK).toInt()
                val indexX = (calcIndex % ITEM_CHUNK).toInt()
                itemsX[itemIndex][indexX] = (itemsX[itemIndex][indexX] - rowMin).toByte()
                used++;
            }
            sumRows[row] -= rowMin * used
        }

    }


    private fun getColMin(col: Int): Int {
        var min = Integer.MAX_VALUE
        for (row in 1 until size) {
            if (!isUsedRow(row)) {
                min = Math.min(min, get(col, row).toInt())
            }
        }
        return min
    }

    fun subMinCols() {
        for (col in 0 until size) {
            if (!isUsedCol(col)) {
                val colMin = getColMin(col)
                if (colMin != 0) {
                    for (row in 0 until size) {
                        val calcIndex = calcIndex(col, row)
                        val itemIndex = (calcIndex / ITEM_CHUNK).toInt()
                        val index = (calcIndex % ITEM_CHUNK).toInt()

                        itemsX[itemIndex][index] = (itemsX[itemIndex][index] - colMin).toByte()
                    }
                } else {
                    //               println("already zero min")
                }
            }
        }
    }

    private fun isUsedRow(row: Int): Boolean {
        return usedByRow[row] != -1;
    }

    private fun isUsedCol(col: Int): Boolean {
        return usedByCol[col] != -1;
    }

    fun refreshZeros() {
        for ((index, value) in zerosInCol.withIndex()) {
            if (value > 0) {
                zerosInCol[index] = 0
            }
        }

        for ((index, value) in zerosInRow.withIndex()) {
            if (value > 0) {
                zerosInRow[index] = 0
            }
            sumRows[index] = 0
        }

        var offset = 0L
        for (item in itemsX) {
            for ((index, value) in item.withIndex()) {
                if (value == 0.toByte()) {
                    zerosInCol[calcCol(offset + index)]++
                    zerosInRow[calcRow(offset + index)]++
                } else {
                    val calcRow = calcRow(offset + index)
                    val calcCol = calcCol(offset + index)
                    if (!isUsedRow(calcRow)) {
                        sumRows[calcRow.toInt()] = sumRows[calcRow.toInt()] + value
                    }
                    if (!isUsedCol(calcCol)) {
                        sumCols[calcCol.toInt()] = sumCols[calcCol.toInt()] + value
                    }
                }
            }
            offset += ITEM_CHUNK
        }
    }

    fun getFirstZeroIndexCol(col: Int): Int {
        if (isUsedCol(col)) {
            return -1
        }
        for (row in 0 until size) {
            if (!isUsedRow(row)) {
                if (get(col, row) == 0.toByte()) {
                    return row
                }
            }
        }
        return -1
    }

    fun getZeroByMaxSum(col: Int): Int {
        if (isUsedCol(col)) {
            return -1
        }

        val min = IntStream.range(0, size).parallel().filter { !isUsedRow(it) }.filter { get(col, it) == 0.toByte() }.boxed()
                .max { row1: Int?, row2: Int? ->
                    Integer.compare(sumRows[row1!!], sumRows[row2!!])
                }
        if (min.isPresent) {
            return min.get()
        }
        return -1
    }

    fun getZeroByMaxInRows(col: Int): Int {
        if (isUsedCol(col)) {
            return -1
        }

        val min = IntStream.range(0, size).parallel().filter { !isUsedRow(it) }.filter { get(col, it) == 0.toByte() }.boxed().max { row1: Int?, row2: Int? ->
            val v1 = zerosInRow[row1!!]
            val v2 = zerosInRow[row2!!]
            Integer.compare(v1, v2)
        }
        if (min.isPresent) {
            return min.get()
        }
        return -1
    }


    fun getZeroByMinInRows(col: Int): Int {
        if (isUsedCol(col)) {
            return -1
        }

        val min = IntStream.range(0, size).parallel().filter { !isUsedRow(it) }.filter { get(col, it) == 0.toByte() }.boxed().min { row1: Int?, row2: Int? ->
            val v1 = if (zerosInRow[row1!!]!! != 0) {
                zerosInRow[row1]
            } else {

                Integer.MAX_VALUE
            }
            val v2 = if (zerosInRow[row2!!]!! != 0) {
                zerosInRow[row2]
            } else {
                Integer.MAX_VALUE
            }
            Integer.compare(v1, v2)
        }
        if (min.isPresent) {
            return min.get()
        }
        return -1
    }

    fun markUsed(col: Int, row: Int) {
        require(!isUsedCol(col)) { "already used column: $col" }
        require(!isUsedRow(row)) { "already used row: $row" }

        usedByRow[row] = col
        usedByCol[col] = row

        zerosInCol[col] = 0
        zerosInRow[row] = 0
        sumRows[row] = 0
        sumCols[col] = 0
    }

}