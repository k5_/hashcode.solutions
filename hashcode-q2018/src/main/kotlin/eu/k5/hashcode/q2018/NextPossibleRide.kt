package eu.k5.hashcode.q2018

class NextPossibleRide(
        val problem: Problem,
        val params: Params
) : Solver {

    val vehicles = ArrayList<Vehicle>()
    val openRides = ArrayList<Problem.Ride>()
    val usedRides = HashSet<Int>()

    init {
        for (vehicle in 0 until problem.vehicles) {
            vehicles.add(Vehicle(vehicle))
        }
        openRides.addAll(problem.rides)
    }

    override fun solve(): Solution {
        val solution = Solution(problem)

        var vehicleUsed = true
        while (vehicleUsed) {
            vehicleUsed = false
            for (v in vehicles) {
                if (v.finished) {
                    continue
                }
                val closestRide = select(v)
                if (closestRide != null) {
                    takeRide(solution, v, closestRide)
                    vehicleUsed = true
                } else {
                    v.finished = true
                }
            }
        }
        return solution
    }

    private fun takeRide(solution: Solution, vehicle: Vehicle, ride: Problem.Ride) {
        val distance = vehicle.location.distance(ride.start)

        vehicle.step += distance
        if (vehicle.step < ride.startStep) {
            vehicle.step = ride.startStep
        } else if (vehicle.step > ride.latestStart) {
            vehicle.finished
            return
        }
        vehicle.step += ride.length
        vehicle.location = ride.target
        solution.addRide(vehicle.id, ride.id)
        usedRides.add(ride.id)
    }

    private fun select(vehicle: Vehicle): Problem.Ride? {
        val closest = selectClosest(vehicle)
        if (!params.considerBonus) {
            return closest
        }
        val withBonus = selectClosesWithBonus(vehicle)

        if (closest != null && withBonus != null) {

            var closestStart = vehicle.step + closest.start.distance(vehicle.location)
            if (closestStart < closest.startStep) {
                closestStart = closest.startStep
            }

            if (closestStart + closest.length + closest.target.distance(withBonus.start) <= withBonus.startStep) {
                return closest
            }
            return withBonus

        }
        return null
    }

    private fun selectClosesWithBonus(vehicle: Vehicle): Problem.Ride? {
        return openRides.filter { !usedRides.contains(it.id) }.filter { it.reachable(vehicle.location, vehicle.step) }
                .minWith(MyComparators.By3DDistanceWithBonus(vehicle.location, vehicle.step, problem.bonus))

    }

    private fun selectClosest(vehicle: Vehicle): Problem.Ride? {
        return openRides.filter { !usedRides.contains(it.id) }.filter { it.reachable(vehicle.location, vehicle.step) }
                .minWith(MyComparators.By3DDistance(vehicle.location, vehicle.step))

    }

}