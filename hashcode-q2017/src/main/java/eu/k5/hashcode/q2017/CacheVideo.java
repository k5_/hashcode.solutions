package eu.k5.hashcode.q2017;

import eu.k5.hashcode.q2017.MainByRequest.VideoEndpoint;

public class CacheVideo {
	private final int videoId;

	private final int videoSize;

	private long totalGain;

	public CacheVideo(int videoId, int videoSize) {
		this.videoId = videoId;
		this.videoSize = videoSize;
	}

	public void add(VideoEndpoint videoEndpoint, int latency) {
		if (latency < 0){
			return;
		}
		long gain = videoEndpoint.getTotalRequests() * latency;

		totalGain += gain;
	}

	public int getVideoId() {
		return videoId;
	}

	public int getVideoSize() {
		return videoSize;
	}

	public long getTotalGain() {
		return totalGain;
	}
}
