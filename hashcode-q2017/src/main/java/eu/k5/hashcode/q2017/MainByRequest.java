package eu.k5.hashcode.q2017;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.stream.Collectors;

import eu.k5.hashcode.q2017.MainByRequest.VideoEndpoint;
import eu.k5.hashcode.q2017.Problem.Endpoint;
import eu.k5.hashcode.q2017.Problem.Request;
import eu.k5.hashcode.q2017.Solution.UsedCache;

public class MainByRequest {
	private static ProblemParser parser = new ProblemParser();

	public static void main(String[] args) throws IOException {

		solve("example");
		solve("me_at_the_zoo");
		solve("trending_today");
		solve("videos_worth_spreading");
		solve("kittens");

	}

	public static void solve(String name) throws IOException {
		Problem problem = parser.parse(name);
		Solution solve = solve(problem);

		try (Writer writer = Files.newBufferedWriter(Paths.get("byRequest",  name + ".out"))) {
			solve.write(writer);
		}
	}

	private static Map<VideoEndpoint, List<Request>> createVideoRequest(List<Request> requests) {
		Map<VideoEndpoint, List<Request>> videoEndpoints = new HashMap<>();
		for (Request request : requests) {
			VideoEndpoint videoEndpoint = new VideoEndpoint(request.getVideo(), request.getEndpoint());
			videoEndpoints.computeIfAbsent(videoEndpoint, (__) -> new ArrayList<>()).add(request);
		}

		return videoEndpoints;
	}

	private static Solution solve(Problem problem) {
///		Solution solution = new Solution(problem);

		return null;
		/*
		 * Map<VideoEndpoint, List<Request>> videoEndpoints =
		 * createVideoRequest(problem.getRequests());
		 * 
		 * List<Integer> collect = videoEndpoints.values().stream().map(l ->
		 * l.size()).sorted() .collect(Collectors.toList());
		 * 
		 * Collections.reverse(collect);
		 * 
		 * System.out.println(collect.get(0));
		 * 
		 * if (true) return solution;
		 */
	
		// List<Request> requests = problem.getRequests();
		// Collections.sort(requests, Main::compareRequests);
		//
		// for (Request request : requests) {
		//
		// int endpoint = request.getEndpoint();
		//
		// Endpoint ep = problem.getEndpoints().get(endpoint);
		//
		// UsedCache useCache = bestCache(solution, ep, request.getVideoSize(),
		// request.getVideo());
		//
		// if (useCache != null) {
		// useCache.addVideo(request.getVideo(), request.getVideoSize());
		// }
		//
		// }
	}

	public static class VideoEndpoint {
		private final int video;
		private final int endpoint;
		private int videoSize;
		private int totalRequests;
		private List<Request> requests;

		public VideoEndpoint(int video, int endpoint) {
			this.video = video;
			this.endpoint = endpoint;
		}

		public VideoEndpoint(int video, int endpoint, int videoSize, int totalRequests, List<Request> requests) {
			this.video = video;
			this.endpoint = endpoint;
			this.videoSize = videoSize;
			this.totalRequests = totalRequests;
			this.requests = requests;

		}

		public int getVideoSize() {
			return videoSize;
		}

		public void setVideoSize(int videoSize) {
			this.videoSize = videoSize;
		}

		public int getTotalRequests() {
			return totalRequests;
		}

		public void setTotalRequests(int totalRequests) {
			this.totalRequests = totalRequests;
		}

		public List<Request> getRequests() {
			return requests;
		}

		public void setRequests(List<Request> requests) {
			this.requests = requests;
		}

		public int getVideo() {
			return video;
		}

		public int getEndpoint() {
			return endpoint;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + endpoint;
			result = prime * result + video;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			VideoEndpoint other = (VideoEndpoint) obj;
			if (endpoint != other.endpoint)
				return false;
			if (video != other.video)
				return false;
			return true;
		}

		public VideoEndpoint withRequests(List<Request> requests) {

			int totalRequests = requests.stream().map(Request::getRequests).mapToInt(Integer::valueOf).sum();
			int videoSize = requests.get(0).getVideoSize();

			return new VideoEndpoint(video, endpoint, videoSize, totalRequests, requests);
		}

	}


}
