package eu.k5.hashcode.q2017;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

import eu.k5.hashcode.q2017.Problem.Endpoint;
import eu.k5.hashcode.q2017.Problem.Request;
import eu.k5.hashcode.q2017.Solution.UsedCache;

public class SolverGreedyByRequest implements Solver {
    private final Problem problem;
    private final Params params;
    private List<Request> requests;
    private boolean shouldRecal = false;

    public SolverGreedyByRequest(Problem problem, Params params) {
        this.problem = problem;
        this.params = params;
        requests = new ArrayList<>(problem.getRequests());
    }

    public Solution solve() {

        Solution solution = new Solution(problem, "greedyByRequest" + (shouldRecal ? "Recalc" : "norecalc"));

        List<Potential> potentials = createPotenials(requests, problem, solution);

        int c = 0;
        int reCalc = recalc(problem.getRequestCount());
        Iterator<Potential> iPot = potentials.iterator();
        for (; iPot.hasNext(); ) {
            Potential p = iPot.next();

            Request request = requests.get(p.getRequest());
            int endpoint = request.getEndpoint();

            Endpoint ep = problem.getEndpoints().get(endpoint);

            UsedCache useCache = bestCache(solution, ep, request.getVideoSize(), request.getVideo());

            if (useCache != null) {
                useCache.addVideo(request.getVideo(), request.getVideoSize());
            }

            requests.set(p.getRequest(), null);
            c++;

            if (shouldRecal && c > reCalc) {
                potentials = createPotenials(problem.getRequests(), problem, solution);
                System.out.println(potentials.size());
                iPot = potentials.iterator();
                c = 0;
                reCalc = recalc(potentials.size());
            }
        }

        return solution;
    }

    private static long calculatePotential(Request request, Problem problem, Solution solution) {
        Endpoint endpoint = problem.getEndpoints().get(request.getEndpoint());

        UsedCache use = bestCache(solution, endpoint, request.getVideoSize(), request.getVideo());
        if (use == null) {
            return 0;
        }

        UsedCache used = bestUsed(solution, endpoint, request.getVideo());
        int usedLatency = endpoint.getCaches().get(use.getId());

        if (used != null) {
            usedLatency = endpoint.getCaches().getOrDefault(used.getId(), 0) - usedLatency;
        }
        usedLatency = endpoint.getLatency() - usedLatency;
        return usedLatency * request.getRequests() * 100 / request.getVideoSize();
    }

    private static int compareRequests(Request r1, Request r2) {
        int byRequests = Integer.compare(r1.getRequests(), r2.getRequests());
        if (byRequests != 0) {
            return byRequests;
        }

        return Integer.compare(r1.getVideoSize(), r2.getVideoSize());
    }

    private static List<Potential> createPotenials(List<Request> requests, Problem problem, Solution solution) {

        return requests.stream().filter(Objects::nonNull)
                .map(r -> new Potential(r.getId(), calculatePotential(r, problem, solution))).sorted()
                .collect(Collectors.toList());

    }

    private static class Potential implements Comparable<Potential> {
        private final int request;
        private final long potential;

        public Potential(int request, long potential) {
            this.request = request;
            this.potential = potential;
        }

        public int getRequest() {
            return request;
        }

        public long getPotential() {
            return potential;
        }

        @Override
        public int compareTo(Potential o) {
            return Long.compare(o.potential, potential);
        }

    }

    private static int recalc(int requests) {
        int rc = requests / 20;
        if (rc < 100) {
            rc = 100;
        }

        return rc;
    }

    private static UsedCache bestUsed(Solution solution, Endpoint endpoint, int video) {
        for (int index = endpoint.getBestCaches().size() - 1; index >= 0; index--) {
            Integer cacheId = endpoint.getBestCaches().get(index);
            UsedCache used = solution.getCache(cacheId);
            if (used.contains(video)) {
                return used;
            }

        }
        return null;
    }

    private static UsedCache bestCache(Solution solution, Endpoint endpoint, int requiredSize, int videoId) {

        for (int index = endpoint.getBestCaches().size() - 1; index >= 0; index--) {
            Integer cacheId = endpoint.getBestCaches().get(index);
            UsedCache used = solution.getCache(cacheId);
            if (used.contains(videoId)) {
                return null;
            }
            if (used.getRemainingSize() >= requiredSize) {
                return used;
            }
        }

        return null;
        // endpoint.getBestCaches().get(index)
    }

}
