package eu.k5.hashcode.f2018;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map.Entry;
import java.util.Optional;

import eu.k5.hashcode.f2018.Problem.Building;

public class SolverOneResidential {
	private Problem problem;

	public SolverOneResidential(Problem problem) {
		this.problem = problem;
	}

	public Building getBestByHeight() {
		Optional<Building> b = problem.getResidential().stream().max(new BuildingValueByHeight());

		System.out.println(b.get());

		return b.get();
	}

	static class BuildingValueByHeight implements Comparator<Building> {

		@Override
		public int compare(Building o1, Building o2) {
			int val1 = o1.getValue() / o1.getHeight();
			int val2 = o2.getValue() / o2.getHeight();
			if (val1 == val2) {
				return Integer.compare(o2.getWidth(), o1.getWidth());
			}
			return Integer.compare(val1, val2);
		}
	}

	static class BuildingValueByWidht implements Comparator<Building> {

		@Override
		public int compare(Building o1, Building o2) {
			int val1 = o1.getWidth();
			int val2 = o2.getWidth();
			if (val1 == val2) {
				return Integer.compare(o1.getHeight(), o2.getHeight());
			}
			return Integer.compare(val1, val2);
		}
	}

	public Building getBestByWidth(List<Building> buildings) {
		Optional<Building> min = buildings.stream().min(new BuildingValueByWidht());
		System.out.println(min.get());
		return min.get();
	}

	public Solution solve() {
		Building resi = getBestByHeight();

		List<Building> b = new ArrayList<>();
		for (Entry<Integer, List<Building>> entry : problem.getBuildings().entrySet()) {
			Building width = getBestByWidth(entry.getValue());
			b.add(width);
		}

		b.sort(new BuildingValueByWidht());

		List<Building> use = new ArrayList<>();
		int size = 0;
		for (Building x : b) {
			size = x.getWidth();
			if (size > problem.getWalking() * 2) {
				break;
			}
			use.add(x);
		}

		List<Building> pattern = new ArrayList<>();
		pattern.add(use.get(use.size() - 1));
		for (int i = 0; i < use.size() - 2; i++) {
			pattern.add(use.get(i));
		}

		return createPattern(resi, pattern);
	}

	private Solution createPattern(Building resi, List<Building> pattern) {

		Solution solution = new Solution();

		int repeat = 0;

		while (true) {

			int col = 0;
			boolean resiDone = false;
			for (Building util : pattern) {
				if (col + repeat + util.getWidth() >= problem.getCols()) {
					return solution;
				}
				for (int row = 0; row < problem.getRows() - util.getHeight(); row += util.getHeight()) {
					solution.addBuilding(row, col + repeat, util.getId(), util, problem);
				}
				col += util.getWidth();
				if (col >= problem.getWalking() && !resiDone) {
					if (col + repeat + resi.getWidth() >= problem.getCols()) {
						return solution;
					}

					for (int row = 0; row < problem.getRows() - resi.getHeight(); row += resi.getHeight()) {
						solution.addBuilding(row, col + repeat, resi.getId(), resi, problem);
					}
					resiDone = true;
					col += resi.getWidth();
				}
			}

			if (col + repeat + resi.getWidth() >= problem.getCols()) {
				return solution;
			}

			for (int row = 0; row < problem.getRows() - resi.getHeight(); row += resi.getHeight()) {
				solution.addBuilding(row, col + repeat, resi.getId(), resi, problem);
			}
			resiDone = true;
			col += resi.getWidth();

			repeat = repeat + col;
		}

	}

}
