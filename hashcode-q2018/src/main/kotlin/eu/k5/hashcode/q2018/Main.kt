package eu.k5.hashcode.q2018

import java.nio.file.Paths

fun main(args: Array<String>) {

    var total = 0L
    total += solve("a_example", Params()) { problem, params -> NextPossibleRide(problem, params) }
    total += solve("b_should_be_easy", Params()) { problem, params -> NextPossibleRide(problem, params) }
    total += solve("c_no_hurry", Params()) { problem, params -> NextPossibleRide(problem, params) }
    total += solve("d_metropolis", Params()) { problem, params -> NextPossibleRide(problem, params) }
    total += solve("e_high_bonus", Params(considerBonus = true)) { problem, params -> NextPossibleRide(problem, params) }


    println("total $total")
}

fun solve(name: String, params: Params, solver: (Problem, Params) -> Solver): Long {

    val problem = Problem.parse(name)

    val sol = solver(problem, params)

    val solution = sol.solve()

    if (params.writeSolution) {
        solution.write(Paths.get("out"))
    }

    val score = solution.score()
    println("$name $score")

    return score

}


class Params(

        val writeSolution: Boolean = true,
        val considerBonus: Boolean = false

)