package eu.k5.hashcode.q2019

import org.junit.Test
import java.util.*
import kotlin.test.assertEquals

class AlgorithmsTest {


    @Test
    fun test() {
        val t1 = Arrays.asList(1, 2).toIntArray()
        val t2 = Arrays.asList(1, 2).toIntArray()
        assertEquals(0, Algorithms.score(t1, t2))
    }

    @Test
    fun test2() {
        val t1 = Arrays.asList(1, 2).toIntArray()
        val t2 = Arrays.asList(1, 3).toIntArray()
        assertEquals(1, Algorithms.score(t1, t2))
    }


    @Test
    fun test3() {
        val t1 = Arrays.asList(0, 1, 2, 3).toIntArray()
        val t2 = Arrays.asList(1, 3, 4, 5).toIntArray()
        assertEquals(2, Algorithms.score(t1, t2))
    }
}