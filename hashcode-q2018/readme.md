=== Solver for Google HashCode 2018 Qualification

Scores

a_example 10
b_should_be_easy 176877
c_no_hurry 15800583
d_metropolis 11805027
e_high_bonus 21465945
total 49248442

would have been place 73

== Solver By Distance
This solver does not support multiple warehouses, so it is only used for "mother_of_all_warehouses" testset


* while at least one vehicle took a new ride

* for each vehicle

  * find closest ride according to space and waiting time
     if it is reachable and not yet used

     * if no new ride was found mark vehicle as finished
     * else take ride
