package eu.k5.hashcode.q2018

class BackwardsRides(
        val problem: Problem
) : Solver {


    val vehicles = ArrayList<Vehicle>()
    val openRides = ArrayList<Problem.Ride>()
    val usedRides = HashSet<Int>()

    init {

        for (vehicle in 0 until problem.vehicles) {
            vehicles.add(Vehicle(vehicle))
        }

        openRides.addAll(problem.rides)
    }

    override fun solve(): Solution {
        return Solution(problem)
    }

}