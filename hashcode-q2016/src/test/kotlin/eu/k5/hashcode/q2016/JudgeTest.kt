package eu.k5.hashcode.q2016

import org.junit.Test
import kotlin.test.assertEquals

class JudgeTest {

    @Test
    fun test() {
        val solution = Solution(Problem.Companion.parse("example"))

        solution.addCommand(0, Solution.Action.LOAD, 0, 0, 1)
        solution.addCommand(0, Solution.Action.LOAD, 0, 1, 1)
        solution.addCommand(0, Solution.Action.DELIVER, 0, 0, 1)
        assertEquals(0L, solution.score())
        solution.addCommand(0, Solution.Action.LOAD, 1, 2, 1)

        solution.addCommand(0, Solution.Action.DELIVER, 0, 2, 1)

        assertEquals(64L, solution.score())


        solution.addCommand(1, Solution.Action.LOAD, 1, 2, 1)
        solution.addCommand(1, Solution.Action.DELIVER, 2, 2, 1)
        assertEquals(144L, solution.score())
        solution.addCommand(1, Solution.Action.LOAD, 0, 0, 1)
        solution.addCommand(1, Solution.Action.DELIVER, 1, 0, 1)
        assertEquals(194L, solution.score())

    }

}