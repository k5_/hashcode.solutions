package eu.k5.hashcode.q2016

import java.util.function.BiFunction

class SolverWithDroneAssignement(
        val problem: Problem,
        val weightWeight: Double,
        val changeWarehouse: Boolean,
        val doubleCheckEmpty: Boolean,
        val exclusive: ExclusiveItems) : Solver {

    private var openOrders: ArrayList<DeliverOrder> = ArrayList()

    private val drones = ArrayList<Drone>()
    private val solution = Solution(problem)

    private val warehouses = ArrayList<LoadWarehouse>()


    init {

        for (order in problem.orders) {
            openOrders.add(DeliverOrder(order.id, order.location, HashSet<Int>(order.items), order.weight))
        }

        openOrders.sortWith(ByDistanceComparator(problem.warehouses[0].location, weightWeight))

        val warehouseCount = problem.warehouses.size

        for (warehouse in problem.warehouses) {
            warehouses.add(LoadWarehouse(warehouse.id, warehouse.location, warehouse.products))
        }

        if (exclusive.use) {
            // check for exclusive items
            for ((product, size) in problem.products.withIndex()) {
                var count = 0
                for (order in problem.orders) {
                    if (order.items.contains(product)) {
                        count += 1
                    }
                }

                if (exclusive.maxSize >= size && size * count > exclusive.minTotal) {
                    //println("total $size $count $product")

                    for (warehous in problem.warehouses) {
                        val c = Math.min(count, warehous.products[product])

                        if (c * size > exclusive.minTotal) {
                            this.warehouses[warehous.id].exclusiveProduct.add(product)
                        }
                    }
                }
            }
        }
        for (drone in 0 until problem.drones) {
            val drone = Drone(drone, 0, problem.warehouses[0].location)
            drones.add(drone)
        }

    }



    override fun solve(): Solution {

        //  ordersByDistance.removeAll { it.weight > problem.capacity }

        while (!openOrders.isEmpty()) {
            for (drone in drones) {

                drone.reset()
                val current = HashSet<Int>()
                var checked = 0


                if (changeWarehouse) {
                    val closestWarehouse = closestWarehouse(drone.location)
                    if (closestWarehouse == null) {
                        return solution
                        //println(openOrders)
                    }
                    drone.warehouse = closestWarehouse ?: 0
                }
                val warehouse: LoadWarehouse = warehouses[drone.warehouse]

                if (!warehouse.exclusiveProduct.isEmpty()) {
                    handleExclusive(drone,  current)
                    checked = 1
                }

                while (checked <= 10) {
                    if (checked == 0) {
                        openOrders = ArrayList(openOrders.sortedWith(ByDistanceComparator(problem.warehouses[drone.warehouse].location, weightWeight)))

                    } else {
                        openOrders = ArrayList(openOrders.sortedWith(ByDistanceComparator(drone.location, weightWeight)))
                    }
                    for (order in openOrders) {
                        if (current.contains(order.id)) {
                            continue
                        }

                        current.add(order.id)
                        var available = false
                        var added = false
                        for (item in order.openItems) {
                            if (warehouse.products[item] != 0) {
                                available = true
                            } else {
                                //continue
                                break
                            }

                            val productWeight = problem.products[item]
                            if (drone.weight + productWeight <= problem.capacity) {
                                deliverPart(drone, order, item)
                                warehouse.products[item] = warehouse.products[item] - 1
                                added = true
                            } else {
                                break
                            }
                        }
                        if (available) {
                            if (added) {
                                break
                            } else {
                                checked++
                            }
                        }
                    }
                    checked++
                }
                val completed = doOrders(drone)
//            executed.addAll(use.map { it.id })
                openOrders.removeAll(completed)


                if (drone.use.isEmpty()) {
                    //      println(drone.weight)

                    if (doubleCheckEmpty) {
                        val empty = isEmpty(warehouse)
                        //  println(warehouse.id.toString() + " " + empty)
                        if (!empty) {
                            val firsts = firsts(warehouse)
                            drone.use.addAll(firsts)
                            val completed = doOrders(drone)
                            openOrders.removeAll(completed)
                        }
                        warehouse.empty = empty
                    } else {

                        warehouse.empty = true
                    }
                }

                if (drone.use.isEmpty()) {
                    drone.warehouse = (drone.warehouse + 1) % problem.warehouses.size
                }
            }
        }

        return solution
    }



    private fun handleExclusive(drone: Drone, current: HashSet<Int>) {
        val product = warehouses[drone.warehouse].exclusiveProduct.min()
        if (product == null) {
            return
        }
        println("handle exlusive product $product")
        val productWeight = problem.products[product]

        val parts = ArrayList<DeliverPart>()
        for (order in openOrders) {
            if (order.openItems.contains(product)) {
                if (drone.weight + productWeight <= problem.capacity && warehouses[drone.warehouse].products[product] < parts.size) {
                    parts.add(DeliverPart(order, product))
                    drone.weight += productWeight
                    current.add(order.id)
                } else {
                    break
                }
            }
        }
        if (parts.size * productWeight < exclusive.minTotal) {
            warehouses[drone.warehouse].exclusiveProduct.remove(product)
            current.clear()
            return
        }

        var location = warehouses[drone.warehouse].location
        while (!parts.isEmpty()) {
            val min = parts.minWith(ByDistancePartComparator(drone.location))!!
            drone.use.add(min)
            location = min.order.location
            parts.remove(min)
        }
        warehouses[drone.warehouse].products[product] -= drone.use.size
        drone.location = location
    }

    private fun isEmpty(warehouse: LoadWarehouse): Boolean {
        for (order in openOrders) {
            for (item in order.openItems) {
                if (warehouse.products[item] != 0) {
                    return false
                }
            }
        }
        return true
    }

    private fun firsts(warehouse: LoadWarehouse): ArrayList<DeliverPart> {
        var weight = 0
        var items = ArrayList<DeliverPart>()
        for (order in openOrders) {
            for (item in order.openItems) {
                if (warehouse.products[item] != 0) {
                    if (weight + problem.products[item] <= problem.capacity) {
                        weight += problem.products[item]
                        warehouse.products[item] = warehouse.products[item] - 1

                        items.add(DeliverPart(order, item))
                    } else {
                        return items
                    }
                }
            }
        }
        return items
    }

    private fun closestWarehouse(location: Location): Int? {
        return warehouses.filter { !it.empty }.minWith(ByWarehouseDistance(location))?.id
    }

    private fun deliverPart(drone: Drone, order: DeliverOrder, item: Int) {
        drone.use.add(DeliverPart(order, item))
        drone.load(problem.warehouses[drone.warehouse].location, problem.products[item])
    }

    private fun doOrders(drone: Drone): ArrayList<DeliverOrder> {
        //   println(parts.size)
        val required = HashMap<Int, Int>()
        for (part in drone.use) {
            required.compute(part.item, BiFunction { _, c -> (c ?: 0).plus(1) })
        }

        for (load in required) {
            solution.load(drone.id, drone.warehouse, load.key, load.value)
        }

        val completed = ArrayList<DeliverOrder>()
        for (part in drone.use) {
            solution.addCommand(drone.id, Solution.Action.DELIVER, part.order.id, part.item, 1)
            part.order.openItems.remove(part.item)
            if (part.order.openItems.isEmpty()) {
                completed.add(part.order)
            }
            drone.deliver(part.order.location)
        }
        return completed
    }

}


class LoadWarehouse(val id: Int, val location: Location, initProducts: List<Int>) {

    val products = ArrayList(initProducts)
    var empty: Boolean = false

    val exclusiveProduct = HashSet<Int>()

}

class ByWarehouseDistance(val location: Location) : Comparator<LoadWarehouse> {
    override fun compare(o1: LoadWarehouse?, o2: LoadWarehouse?): Int {
        val d1 = o1!!.location.distance(location)
        val d2 = o2!!.location.distance(location)
        return Integer.compare(d1, d2)
    }
}

