package eu.k5.hashcode.q2019

class SolverStraight(private val problem: Problem) : Solver {


    override fun solve(): Solution {
        val solution = Solution(problem)

        for (photo in problem.photos) {
            if (photo.horizontal) {
                solution.addSlide(Problem.Slide(photo))
            }
        }

        return solution
    }

}