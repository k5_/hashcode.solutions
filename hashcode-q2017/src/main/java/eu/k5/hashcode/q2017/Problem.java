package eu.k5.hashcode.q2017;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

public class Problem {
	private final String name;
	private final int videoCount;
	private final int endpointCount;
	private final int requestCount;
	private final int cacheCount;
	private final int cacheCapacity;
	private final int[] videos;
	private final List<Endpoint> endpoints;
	private final List<Request> requests;

	public Problem(String name, String header, int[] videos) {
		this.name = name;
		String[] parts = header.split(" ");

		this.videoCount = Integer.parseInt(parts[0]);
		this.endpointCount = Integer.parseInt(parts[1]);
		this.requestCount = Integer.parseInt(parts[2]);
		this.cacheCount = Integer.parseInt(parts[3]);
		this.cacheCapacity = Integer.parseInt(parts[4]);
		this.videos = videos;
		this.endpoints = Collections.emptyList();
		this.requests = Collections.emptyList();
	}

	private Problem(Problem problem, List<Endpoint> endpoints, List<Request> requests) {

		this.name = problem.name;
		this.videoCount = problem.videoCount;
		this.endpointCount = problem.endpointCount;
		this.requestCount = problem.requestCount;
		this.cacheCount = problem.cacheCount;
		this.cacheCapacity = problem.cacheCapacity;
		this.videos = problem.videos;
		this.requests = Collections.unmodifiableList(requests);
		this.endpoints = endpoints;
	}

	public static class Endpoint {

		private final int endpointId;
		private final int latency;
		// cacheid -> latency
		private final Map<Integer, Integer> caches;

		private final List<Integer> bestCaches;

		public Endpoint(int endpointId, int latency, Map<Integer, Integer> caches) {
			this.endpointId = endpointId;
			this.latency = latency;
			this.caches = caches;

			List<Integer> list = caches.entrySet().stream().sorted((e1, e2) -> e2.getValue().compareTo(e1.getValue()))
					.map(e -> e.getKey()).collect(Collectors.toList());
			bestCaches = new ArrayList<>(list);
		}

		public int getEndpointId() {
			return endpointId;
		}

		public int getLatency() {
			return latency;
		}

		public Map<Integer, Integer> getCaches() {
			return caches;
		}

		public List<Integer> getBestCaches() {
			return bestCaches;
		}

		public int getBestCacheLatency() {
			if (bestCaches.isEmpty()) {
				return 0;
			}
			return latency - caches.get(bestCaches.get(0));
		}

		public int getCacheLatency(int cacheId) {

			Integer latency = caches.get(cacheId);
			if (latency == null) {
				return 0;
			}
			return this.latency - latency;
		}

		public int getLatency(int id) {
			Integer latency = caches.get(id);
			if (latency != null) {
				return this.latency - latency;
			}
			return 0;
		}

	}

	public static class Request {

		private final int video;
		private final int endpoint;
		private final int requests;
		private final int videoSize;
		private final int id;

		public Request(int id, int video, int endpoint, int requests, int videoSize) {
			this.id = id;
			this.video = video;
			this.endpoint = endpoint;
			this.requests = requests;
			this.videoSize = videoSize;
		}

		public int getId() {
			return id;
		}

		public int getVideoSize() {
			return videoSize;
		}

		public int getVideo() {
			return video;
		}

		public int getEndpoint() {
			return endpoint;
		}

		public int getRequests() {
			return requests;
		}

	}

	public String getName() {
		return name;
	}

	public int getVideoCount() {
		return videoCount;
	}

	public int getEndpointCount() {
		return endpointCount;
	}

	public int getRequestCount() {
		return requestCount;
	}

	public int getCacheCount() {
		return cacheCount;
	}

	public int getCacheCapacity() {
		return cacheCapacity;
	}

	public int[] getVideos() {
		return videos;
	}

	public List<Endpoint> getEndpoints() {
		return endpoints;
	}

	public List<Request> getRequests() {
		return requests;
	}

	public Problem withEndpointsAndRequests(List<Endpoint> endpoints, List<Request> requests) {
		return new Problem(this, endpoints, requests);
	}

}