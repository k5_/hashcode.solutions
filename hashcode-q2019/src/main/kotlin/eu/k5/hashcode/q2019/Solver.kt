package eu.k5.hashcode.q2019

interface Solver {

    fun solve(): Solution

}