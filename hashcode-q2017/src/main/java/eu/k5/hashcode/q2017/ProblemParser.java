package eu.k5.hashcode.q2017;

import java.io.BufferedReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.k5.hashcode.q2017.Problem.Endpoint;
import eu.k5.hashcode.q2017.Problem.Request;

public class ProblemParser {

	public Problem parse(String name) {

		try (BufferedReader reader = Files.newBufferedReader(Paths.get("in", name + ".in"))) {
			return parse(name, reader);
		} catch (IOException e) {
			throw new RuntimeException(e);
		}
	}

	public Problem parse(String name, BufferedReader reader) throws IOException {
		String header = reader.readLine();
		String videos = reader.readLine();

		Problem problem = new Problem(name, header, parseVideos(videos));

		List<Endpoint> endpoints = parseEndpoints(problem.getEndpointCount(), reader);

		List<Request> requests = parseRequests(problem.getRequestCount(), reader, problem.getVideos());

		problem = problem.withEndpointsAndRequests(endpoints, requests);
		return problem;

	}

	private int[] parseVideos(String videos) {
		String[] parts = videos.split(" ");
		int[] v = Arrays.stream(parts).map(Integer::parseInt).mapToInt(Integer::intValue).toArray();

		return v;
	}

	private List<Request> parseRequests(int requestCount, BufferedReader reader, int[] videos) throws IOException {

		List<Request> requests = new ArrayList<>();

		for (int requestId = 0; requestId < requestCount; requestId++) {
			requests.add(parseRequest(reader, requestId, videos));
		}
		return requests;
	}

	private Request parseRequest(BufferedReader reader, int id, int[] videos) throws IOException {
		String request = reader.readLine();
		String[] parts = request.split(" ");

		int video = Integer.parseInt(parts[0]);
		int endpoint = Integer.parseInt(parts[1]);
		int requests = Integer.parseInt(parts[2]);
		return new Request(id, video, endpoint, requests, videos[video]);
	}

	private List<Endpoint> parseEndpoints(int endpointCount, BufferedReader reader) throws IOException {
		List<Endpoint> endpoints = new ArrayList<>();

		for (int endpointId = 0; endpointId < endpointCount; endpointId++) {
			endpoints.add(parseEndpoint(endpointId, reader));
		}
		return endpoints;
	}

	private Endpoint parseEndpoint(int endpointId, BufferedReader reader) throws IOException {
		String endpointHeader = reader.readLine();
		String[] parts = endpointHeader.split(" ");
		int latency = Integer.parseInt(parts[0]);
		int cacheLatencies = Integer.parseInt(parts[1]);

		Map<Integer, Integer> caches = parseLatencies(cacheLatencies, reader);
		return new Endpoint(endpointId, latency, caches);
	}

	private Map<Integer, Integer> parseLatencies(int latencyCount, BufferedReader reader) throws IOException {
		Map<Integer, Integer> caches = new HashMap<>(latencyCount);
		for (int index = 0; index < latencyCount; index++) {

			String[] latencyParts = reader.readLine().split(" ");
			int id = Integer.parseInt(latencyParts[0]);
			int clatency = Integer.parseInt(latencyParts[1]);
			caches.put(id, clatency);
		}
		return caches;
	}

}
