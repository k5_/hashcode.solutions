package eu.k5.hashcode.q2018

class Judge(val problem: Problem, val solution: Solution) {
    private val usedRides = HashSet<Int>()

    fun score(): Long {
        usedRides.clear()
        var totalScore = 0L
        for (vehicle in solution.vehicles) {
            totalScore += scoreVehicle(vehicle)
        }
        return totalScore
    }

    fun scoreVehicle(vehicle: Solution.VehicleRides): Long {
        var score = 0L
        var location = Location(0, 0)
        var step = 0
        for (rideId in vehicle.rides) {
            if (usedRides.contains(rideId)) {
                throw IllegalArgumentException("Already used ride $rideId")
            }
            usedRides.add(rideId)

            val ride = problem.rides[rideId]
            val distance = ride.start.distance(location)
            step += distance
            if (step <= ride.startStep) {
                step = ride.startStep
                score += problem.bonus
            }
            step += ride.length
            if (step <= ride.finishStep) {
                score += ride.length
            }
            location = ride.target

        }
        return score
    }
}