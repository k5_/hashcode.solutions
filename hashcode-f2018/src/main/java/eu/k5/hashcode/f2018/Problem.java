package eu.k5.hashcode.f2018;

import java.util.List;
import java.util.Map;

public class Problem {

	private final int rows;
	private final int cols;
	private final int building;
	private final int walking;
	private final List<Building> residential;

	private final Map<Integer, List<Building>> buildings;

	public Problem(int rows, int cols, int building, int walking, List<Building> residential,
			Map<Integer, List<Building>> buildings) {
		this.rows = rows;
		this.cols = cols;
		this.building = building;
		this.walking = walking;
		this.residential = residential;
		this.buildings = buildings;
	}

	public int getRows() {
		return rows;
	}

	public int getCols() {
		return cols;
	}

	public int getBuilding() {
		return building;
	}

	public int getWalking() {
		return walking;
	}

	public Map<Integer, List<Building>> getBuildings() {
		return buildings;
	}

	public List<Building> getResidential() {
		return residential;
	}

	public static class Building {

		private final boolean residential;
		private final int height;
		private final int width;
		private final int value;
		private final List<Occupy> occupies;
		private final int id;

		public Building(int id, boolean residential, int height, int width, int value, List<Occupy> occupies) {
			this.id = id;
			this.residential = residential;
			this.height = height;
			this.width = width;
			this.value = value;
			this.occupies = occupies;
		}

		public boolean isResidential() {
			return residential;
		}

		public int getHeight() {
			return height;
		}

		public int getWidth() {
			return width;
		}

		public int getValue() {
			return value;
		}

		public List<Occupy> getOccupies() {
			return occupies;
		}

		@Override
		public String toString() {
			StringBuilder builder = new StringBuilder();
			if (residential) {
				builder.append("R").append(Integer.toString(value));
			} else {
				builder.append("U").append(Integer.toString(value));
			}
			builder.append(" ").append(width).append("x").append(height);
			return builder.toString();
		}

		public int getId() {
			return id;
		}
	}

	public static class Occupy {
		private final int row;
		private final int col;

		public Occupy(int row, int col) {
			this.row = row;
			this.col = col;
		}

		public int getRow() {
			return row;
		}

		public int getCol() {
			return col;
		}

	}
}
