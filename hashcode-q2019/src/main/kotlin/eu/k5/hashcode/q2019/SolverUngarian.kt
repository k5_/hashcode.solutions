package eu.k5.hashcode.q2019

import java.util.concurrent.atomic.AtomicInteger
import java.util.stream.IntStream


class SolverUngarian(private val problem: Problem) : Solver {

    val slides = ArrayList<Problem.Slide>()

    override fun solve(): Solution {

        val solution = Solution(problem)

        val photos = problem.photos.filter { it.horizontal }
        for (photo in photos) {
            slides.add(Problem.Slide(photo))
        }

        val combiner = CombinerUngarian(problem)

        slides.addAll(combiner.combine())


        for (chunk in 0 until problem.photos.size step 20000) {
            val matrix = initMatrix(chunk, 20000)

            if (matrix.size <= 2) {
                break
            }
            println(matrix.size)

            val lastCol = optimize(matrix)
            println("adding to solution")
            addToSolution(lastCol, solution, matrix)
            println("score " + solution.score())
        }


        return solution
    }

    private fun optimize(matrix: Matrix): Int {
        var used = 0
        var lastCol = -1
        while (used < matrix.size) {

            matrix.subMinCols()
            matrix.subMinRows()
            matrix.refreshZeros()

            var added = 0
            while (true) {
                val colCell = matrix.zerosInCol.withIndex().filter { it.value > 0 }.minBy { it.value }
                if (colCell != null) {
                    val rowIndex = matrix.getZeroByMaxSum(colCell.index)
                    if (rowIndex != -1) {
                        matrix.markUsed(colCell.index, rowIndex)
                        lastCol = colCell.index
                        used++
                        added++
                        matrix.zerosInRow[rowIndex]=0
                    }
                    matrix.zerosInCol[colCell.index] = 0
                } else {
                    break
                }
                if (added > matrix.size / 20) {
                    println("added" + added + "used" + used)
                    matrix.refreshZeros()
                    added = 0
                }
            }
            //println("" + matrix.size + " " + used)
        }
        return lastCol
    }

    private fun addToSolution(seed: Int, solution: Solution, matrix: Matrix) {
        var lastCol = seed
        solution.addSlide(matrix.photoMap[lastCol]!!)
        var use = 1
        while (use < matrix.size) {
            lastCol = matrix.usedByCol[lastCol]
            val photo = matrix.photoMap[lastCol]!!
            if (!solution.isUsed(photo)) {
                solution.addSlide(photo)
                use++
            } else {
                for (col in 0 until matrix.size) {
                    if (!solution.isUsed(matrix.photoMap[col]!!)) {
                        lastCol = col
                        break
                    }
                }
                //println(use)
            }
        }
    }


    private fun initMatrix(offset: Int, length: Int): Matrix {

        val size = Math.min(slides.size - offset, length)
        if (size < 0) {
            return Matrix(0)
        }
        println(size)
        val matrix = Matrix(size)
        for (index in 0 until matrix.size) {
            matrix.photoMap[index] = slides[index + offset]
        }

        var max = AtomicInteger()
        IntStream.range(0, matrix.size).parallel().forEach {
            val index1 = it
            val photo1 = matrix.photoMap[index1]!!

            for (index2 in index1 + 1 until matrix.size) {
                val photo2 = matrix.photoMap[index2]!!

                val score = Algorithms.score(photo1.tags, photo2.tags)
                matrix.set(index1, index2, score.toByte())
                matrix.set(index2, index1, score.toByte())

                var lmax = max.get()
                if (score > lmax) {

                    while (!max.compareAndSet(lmax, score)) {
                        lmax = max.get()
                        if (score <= lmax) {
                            break
                        }
                    }
                }
            }
        }
        /*  for (index1 in 0 until matrix.size) {


          }*/

        matrix.diff(max.get())
        return matrix
    }

    enum class State {
        INIT, USE
    }

}