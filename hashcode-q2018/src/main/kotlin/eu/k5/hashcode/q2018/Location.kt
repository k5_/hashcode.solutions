package eu.k5.hashcode.q2018

class Location(
        val row: Int,
        val column: Int
) {

    fun distance(location: Location): Int {
        return abs(location.row - row) + abs(location.column - column)
    }

    private fun abs(value: Int): Int {
        if (value < 0) {
            return -value
        } else {
            return value
        }
    }
}