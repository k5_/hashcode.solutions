package eu.k5.hashcode.q2017;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class ProblemParserTest {

	private ProblemParser parser;

	@Before
	public void init() {

		parser = new ProblemParser();
	}

	@Test
	public void test() {
		Problem example = parser.parse("example");

		assertEquals(5, example.getVideoCount());
		assertEquals(5, example.getVideos().length);

		assertEquals(4, example.getRequestCount());
		assertEquals(4, example.getRequests().size());
		assertEquals(1, example.getRequests().get(3).getVideo());
		
		assertEquals(2, example.getEndpointCount());

		assertEquals(2, example.getEndpoints().size());
		assertTrue(example.getEndpoints().get(1).getCaches().isEmpty());
		assertEquals(200, (int) example.getEndpoints().get(0).getCaches().get(2));
		
	}
}
