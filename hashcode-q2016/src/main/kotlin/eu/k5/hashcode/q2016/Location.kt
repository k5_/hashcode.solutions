package eu.k5.hashcode.q2016

class Location(val row: Int, val col: Int) {


    fun distance(target: Location): Int {
        if (target.col == col && target.row == row) {
            return 0
        }

        val x = (target.col - col)
        val y = target.row - row

        return Math.ceil(Math.sqrt((x * x + y * y).toDouble())).toInt()

    }

    fun gradient(target: Location, pivot: Location): Int {
        return 0


    }
}