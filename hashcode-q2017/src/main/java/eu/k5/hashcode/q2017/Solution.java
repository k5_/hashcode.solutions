package eu.k5.hashcode.q2017;

import java.io.IOException;
import java.io.Writer;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import eu.k5.hashcode.q2017.Problem.Endpoint;
import eu.k5.hashcode.q2017.Problem.Request;

public class Solution {
	private Map<Integer, UsedCache> usedCaches = new HashMap<>();
	private int capacity;
	private Problem problem;
	private String type;

	public Problem getProblem() {
		return problem;
	}

	public static class UsedCache {
		private int remainingSize;
		private List<Integer> videos = new ArrayList<>();
		private int id;

		public UsedCache(int id, int remainingSize) {
			this.id = id;
			this.remainingSize = remainingSize;
		}

		public int getRemainingSize() {
			return remainingSize;
		}

		public int getId() {
			return id;
		}

		public void addVideo(int videoId, int videoSize) {
			remainingSize = remainingSize - videoSize;
			videos.add(videoId);
		}

		public boolean contains(int videoId) {
			return videos.contains(videoId);
		}

	}

	public Solution(Problem problem, String type) {
		this.problem = problem;
		this.type = type;
		this.capacity = problem.getCacheCapacity();
	}

	public String getType() {
		return type;
	}

	public UsedCache getCache(int id) {
		return usedCaches.computeIfAbsent(id, (c) -> new UsedCache(c, capacity));
	}

	public void write(Writer writer) throws IOException {

		writer.write("" + usedCaches.size());
		writer.write("\n");
		for (UsedCache cache : usedCaches.values()) {
			writer.write("" + cache.id);
			for (Integer v : cache.videos) {
				writer.write(' ');
				writer.write(v.toString());
			}
			writer.write("\n");
		}
	}

	public void add(Map<Integer, Set<Integer>> useVideos) {
		for (Entry<Integer, Set<Integer>> entry : useVideos.entrySet()) {

			for (int v : entry.getValue()) {
				getCache(entry.getKey()).addVideo(v, 0);
			}
		}
	}

	public long judge() {
		Map<Integer, List<Integer>> byVideo = getByVideo();

		long score = 0l;
		long requests = 0L;
		for (Request request : problem.getRequests()) {

			score += judgeRequest(request, byVideo);
			requests += request.getRequests();
		}

		return (score * 1000) / (requests);

	}

	private long judgeRequest(Request request, Map<Integer, List<Integer>> byVideo) {

		if (!byVideo.containsKey(request.getVideo())) {
			return 0l;
		}

		Endpoint endpoint = problem.getEndpoints().get(request.getEndpoint());

		long score = 0l;

		for (int cache : byVideo.get(request.getVideo())) {

			int cacheLatency = endpoint.getCacheLatency(cache);

			long saved = cacheLatency * request.getRequests();
			score = Math.max(score, saved);
		}
		return score;
	}

	private Map<Integer, List<Integer>> getByVideo() {
		Map<Integer, List<Integer>> byVideo = new HashMap<>();

		for (Entry<Integer, UsedCache> used : this.usedCaches.entrySet()) {

			for (int v : used.getValue().videos) {

				byVideo.computeIfAbsent(v, __ -> new ArrayList<>()).add(used.getKey());
			}
		}
		return byVideo;
	}
}
