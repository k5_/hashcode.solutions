package eu.k5.hashcode.q2017;

import java.io.IOException;
import java.io.Writer;
import java.nio.file.Files;
import java.nio.file.Paths;

public class Main {

    private static ProblemParser parser = new ProblemParser();

    public static void main(String[] args) throws IOException {

        long total = 0L;
        //solve("example", Params.builder().build());
        total += solve("me_at_the_zoo", Params.builder().setSizeWeight(8000).build());
        total += solve("trending_today", Params.builder().build());
        total += solve("videos_worth_spreading", Params.builder().setSizeWeight(45050).build());
        total += solve("kittens", Params.builder().setVerbosity(0).setSizeWeight(130000).build());

        System.out.println("Total " + total);
    }

    public static long solve(String name, Params params) throws IOException {
        Problem problem = parser.parse(name);
        Solution solve = solve(problem, params);

        if (params.isWriteSolution()) {
            Files.createDirectories(Paths.get(solve.getType()));
            try (Writer writer = Files.newBufferedWriter(Paths.get(solve.getType(), name + ".out"))) {
                System.out.println("Write " + name + " for " + solve.getType() + " score " + solve.judge());

                solve.write(writer);
            }
        }

        long score = solve.judge();

        System.out.println("Write " + name + " for " + solve.getType() + " score " + solve.judge());

        return score;
    }

    private static Solution solve(Problem problem, Params params) {
        Solver solver = createSolver(problem, params);

        return solver.solve();
    }

    private static Solver createSolver(Problem problem, Params params) {
        return new SolverGreedyByVideo(problem, params);
    }

}
