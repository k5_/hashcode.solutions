package eu.k5.hashcode.q2016

import org.junit.Test
import kotlin.test.assertEquals

class ProblemTest {

    @Test
    fun example() {
        val problem = Problem.parse("example")

        // 400 600 30 112993 200
        assertEquals(100, problem.rows)
        assertEquals(100, problem.columns)
        assertEquals(3, problem.drones)
        assertEquals(50, problem.deadline)
        assertEquals(500, problem.capacity)

        assertEquals(2, problem.warehouses.size)

        assertEquals(5, problem.warehouses[1].row)

        assertEquals(5, problem.warehouses[1].column)

    }


    @Test
    fun busyDay() {
        val problem = Problem.parse("busy_day")

        // 400 600 30 112993 200
        assertEquals(400, problem.rows)
        assertEquals(600, problem.columns)
        assertEquals(30, problem.drones)
        assertEquals(112993, problem.deadline)
        assertEquals(200, problem.capacity)

        assertEquals(10, problem.warehouses.size)

        assertEquals(297, problem.warehouses[9].row)

    }

}