package eu.k5.hashcode.q2019

fun main(args: Array<String>) {
    var total = 0L
    //total += solve("c_memorable_moments") { SolverUngarian(it) }
    total += solve("c_memorable_moments") { SolverUngarian(it) }
    total += solve("b_lovely_landscapes") { SolverUngarian(it) }
    total += solve("d_pet_pictures") { SolverUngarian(it) }
    //total += solve("e_shiny_selfies") { SolverUngarian(it) }

    println("total " + total)
}


fun solve(name: String, solver: (Problem) -> Solver): Long {
    val problem = Problem.parse(name)
    // analyseProduct(problem)
    //74278 warehouse SolverByDIstance

    val solver = solver(problem)
    val solution = solver.solve()

    val score = solution.score()
    println(name + " " + score)

    return score
//    checkForDoubleItem(problem)
}
