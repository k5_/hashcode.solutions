package eu.k5.hashcode.q2016

class Solution(val problem: Problem) {

    val commands = ArrayList<Command>()


    fun score(): Long {

        return Judge(problem, this).score()

    }

    fun addCommand(drone: Int, action: Solution.Action, target: Int, product: Int, amount: Int) {

        commands.add(Command(drone, action, target, product, amount))
    }

    fun load(drone: Int, warehouse: Int, product: Int, amount: Int) {
        addCommand(drone, Action.LOAD, warehouse, product, amount)
    }

    class Command(
            val drone: Int,
            val action: Action,
            val target: Int,
            val product: Int,
            val amount: Int
    )

    enum class Action {
        LOAD, DELIVER, UNLOAD, WAIT
    }
}