package eu.k5.hashcode.q2016

import java.io.BufferedReader
import java.nio.file.Files
import java.nio.file.Paths

class Problem(val name: String, val rows: Int, val columns: Int, val drones: Int, val deadline: Int, val capacity: Int) {

    var products: List<Int> = ArrayList()

    var warehouses: List<Warehouse> = ArrayList()

    var orders: List<Order> = ArrayList()

    companion object {
        fun parse(name: String): Problem {
            return Files.newBufferedReader(Paths.get("in", "$name.in")).use {
                parse(name, it)
            }
        }

        private fun parse(name: String, reader: BufferedReader): Problem {
            val header = reader.readLine()
            val problem = parseHeader(name, header)

            val productCount = reader.readLine()
            val products = reader.readLine().split(" ")

            problem.products = products.map { Integer.parseInt(it) }

            problem.warehouses = parseWarehouses(reader)
            problem.orders = parseOrders(problem, reader)

            return problem
        }

        private fun parseWarehouses(reader: BufferedReader): List<Warehouse> {
            val warehouseCount = Integer.parseInt(reader.readLine())

            val warehouses = ArrayList<Warehouse>()
            for (warehouse in 0 until warehouseCount) {
                val locationParts = reader.readLine().split(" ")

                val storedProducts = reader.readLine().split(" ")

                val row = Integer.parseInt(locationParts[0])
                val column = Integer.parseInt(locationParts[1])


                val products = storedProducts.map { Integer.parseInt(it) }

                warehouses.add(Warehouse(warehouse,row, column, products))

            }
            return warehouses
        }

        private fun parseOrders(problem: Problem, reader: BufferedReader): ArrayList<Order> {
            val orderCount = Integer.parseInt(reader.readLine())
            val orders = ArrayList<Order>()
            for (order in 0 until orderCount) {
                val location = reader.readLine().split(" ")
                val row = Integer.parseInt(location[0])
                val col = Integer.parseInt(location[1])


                val itemCount = Integer.parseInt(reader.readLine())

                var weight = 0
                val items = HashSet<Int>()
                val products = reader.readLine().split(" ").map { Integer.parseInt(it) }
                for (product in products) {
                    items.add(product)
                    weight += problem.products[product]
                }

                //items.put(product[0], product[1])


                orders.add(Order(order, row, col, items, weight))
            }
            return orders
        }

        private fun parseHeader(name: String, header: String): Problem {
            val parts = header.split(" ")

            val rows = Integer.parseInt(parts[0])
            val columns = Integer.parseInt(parts[1])
            val drones = Integer.parseInt(parts[2])
            val deadline = Integer.parseInt(parts[3])
            val load = Integer.parseInt(parts[4])

            return Problem(name, rows, columns, drones, deadline, load)
        }


    }


    class Warehouse(
            val id:Int,
            val row: Int,
            val column: Int,
            val products: List<Int>
    ) {

        val location: Location = Location(row, column)

    }

    class Order(val id: Int, row: Int, col: Int, val items: Set<Int>, val weight: Int) {
        val location = Location(row, col)
    }
}


