package eu.k5.hashcode.q2018

class Vehicle(
        val id: Int
) {

    var location = Location(0, 0)
    var step = 0
    var finished = false
}