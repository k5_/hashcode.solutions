package eu.k5.hashcode.q2018

import java.io.BufferedReader
import java.nio.file.Files
import java.nio.file.Paths


class Problem(
        val name: String,
        val rows: Int,
        val columns: Int,
        val vehicles: Int,
        val ridesCount: Int,
        val bonus: Int,
        val steps: Int
) {


    class Ride(
            val id: Int,
            startRow: Int,
            startColumn: Int,
            targetRow: Int,
            targetColumn: Int,
            val startStep: Int,
            val finishStep: Int) {

        fun reachable(location: Location, step: Int): Boolean {
            return location.distance(start) + step <= latestStart
        }

        val start = Location(startRow, startColumn)
        val target = Location(targetRow, targetColumn)
        val length = start.distance(target)

        val latestStart = finishStep - length
    }

    var rides: ArrayList<Problem.Ride> = ArrayList()

    companion object {

        fun parse(name: String): Problem {

            return Files.newBufferedReader(Paths.get("in", "$name.in")).use {
                parse(name, it)
            }
        }

        fun parse(name: String, reader: BufferedReader): Problem {

            val header = reader.readLine()
            val problem = parseHeader(name, header)
            problem.rides = parseRides(problem.ridesCount, reader)
            return problem
        }

        fun parseRides(rideCount: Int, reader: BufferedReader): ArrayList<Ride> {

            val rides = ArrayList<Ride>()
            for (ride in 0 until rideCount) {
                val rideString = reader.readLine()
                rides.add(parseRide(ride, rideString))
            }
            return rides

        }


        private fun parseRide(id: Int, rideString: String): Ride {
            val parts = rideString.split(" ")

            val startRow = Integer.parseInt(parts[0])
            val startColumn = Integer.parseInt(parts[1])
            val targetRow = Integer.parseInt(parts[2])
            val targetColumn = Integer.parseInt(parts[3])
            val startStep = Integer.parseInt(parts[4])
            val finishStep = Integer.parseInt(parts[5])

            return Ride(id, startRow, startColumn, targetRow, targetColumn, startStep, finishStep)
        }

        private fun parseHeader(name: String, header: String): Problem {
            val parts = header.split(" ")

            val rows = Integer.parseInt(parts[0])
            val columns = Integer.parseInt(parts[1])
            val vehicles = Integer.parseInt(parts[2])
            val rides = Integer.parseInt(parts[3])
            val bonus = Integer.parseInt(parts[4])
            val steps = Integer.parseInt(parts[5])

            return Problem(name, rows, columns, vehicles, rides, bonus, steps)
        }
    }
}

