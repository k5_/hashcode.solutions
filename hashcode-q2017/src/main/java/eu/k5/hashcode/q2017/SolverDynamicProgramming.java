package eu.k5.hashcode.q2017;

import java.util.ArrayList;
import java.util.BitSet;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.stream.Collectors;

import eu.k5.hashcode.q2017.MainByRequest.VideoEndpoint;
import eu.k5.hashcode.q2017.Problem.Endpoint;
import eu.k5.hashcode.q2017.Problem.Request;

public class SolverDynamicProgramming implements Solver {
	private final Problem problem;
	private final Params params;

	private List<VideoEndpoint> videoEndpoints;

	private Map<Integer, List<Endpoint>> cacheEndpoints;

	private Map<Integer, Set<Integer>> useVideos = new HashMap<>();


	public SolverDynamicProgramming(Problem problem, Params params) {
		this.problem = problem;
		this.params = params;
	}

	private List<VideoEndpoint> getVideoEndpoints() {
		if (videoEndpoints != null) {
			return videoEndpoints;
		}
		Map<VideoEndpoint, List<Request>> videoEndpoints = new HashMap<>();
		for (Request request : problem.getRequests()) {
			VideoEndpoint videoEndpoint = new VideoEndpoint(request.getVideo(), request.getEndpoint());
			videoEndpoints.computeIfAbsent(videoEndpoint, (__) -> new ArrayList<>()).add(request);
		}
		this.videoEndpoints = new ArrayList<>();
		for (Entry<VideoEndpoint, List<Request>> videoEndpoint : videoEndpoints.entrySet()) {

			this.videoEndpoints.add(videoEndpoint.getKey().withRequests(videoEndpoint.getValue()));
		}
		return this.videoEndpoints;
	}

	private Map<Integer, List<Endpoint>> getCacheEndpoints() {
		if (cacheEndpoints != null) {
			return cacheEndpoints;
		}
		cacheEndpoints = new HashMap<>();
		for (Endpoint endpoint : problem.getEndpoints()) {
			for (int cacheId : endpoint.getCaches().keySet()) {
				cacheEndpoints.computeIfAbsent(cacheId, (__) -> new ArrayList<>()).add(endpoint);
			}
		}
		return cacheEndpoints;
	}

	private List<CacheVideo> getCacheVideos(int cacheId) {
		List<Endpoint> list = getCacheEndpoints().get(cacheId);
		Map<Integer, CacheVideo> byVideo = new HashMap<>();
		for (Endpoint endpoint : list) {
			List<VideoEndpoint> videoEndpoints = getVideoEndpoints().stream()
					.filter(ve -> endpoint.getEndpointId() == ve.getEndpoint()).collect(Collectors.toList());

			int epLatency = endpoint.getLatency();
			int latency = epLatency - endpoint.getCaches().get(cacheId);

			for (VideoEndpoint videoEndpoint : videoEndpoints) {

				int bestUsedLatency = getBestUsed(endpoint, videoEndpoint);

				byVideo.computeIfAbsent(videoEndpoint.getVideo(),
						videoId -> new CacheVideo(videoId, videoEndpoint.getVideoSize()))
						.add(videoEndpoint, latency - bestUsedLatency);

			}
		}
		return new ArrayList<>(byVideo.values());
	}

	private int getBestUsed(Endpoint endpoint, VideoEndpoint videoEndpoint) {

		for (int cacheId : endpoint.getBestCaches()) {
			if (useVideos.containsKey(cacheId)) {
				if (useVideos.get(cacheId).contains(videoEndpoint.getVideo())) {

					return endpoint.getCacheLatency(cacheId);

				}
			}
		}
		return 0;
	}

	public Solution solve() {

		for (int i = 0; i < params.getDpRecalc(); i++) {

			if (i == 1) {
				for (int cacheId = 0; cacheId < problem.getCacheCount(); cacheId++) {
					if (cacheId % 2 == 0) {
						useVideos.put(cacheId, new HashSet<>());
					}
				}
			} else if (i == 2) {
				for (int cacheId = 0; cacheId < problem.getCacheCount(); cacheId++) {
					if (cacheId % 2 == 1) {
						useVideos.put(cacheId, new HashSet<>());
					}
				}
			}

			for (int cacheId = 0; cacheId < problem.getCacheCount(); cacheId++) {
				if (i == 1 && cacheId == 0) {
					cacheId += problem.getCacheCount() / 2;
				}
				useVideos.put(cacheId, new HashSet<>());
				List<CacheVideo> cache = getCacheVideos(cacheId);
				int size = requiredSize(cache);
				long gain = maxGain(cache);
				// System.out.printf("%s %s %s %n", cacheId, size, gain);

				Set<Integer> usedVideos = optimize(cache);

				useVideos.put(cacheId, usedVideos);
			}

			Solution solution = new Solution(problem, "dp");
			solution.add(useVideos);
			System.out.println(problem.getName() + " score: " + i + " " + solution.judge());
		}

		Solution solution = new Solution(problem, "dp");
		solution.add(useVideos);
		return solution;
	}

	private int requiredSize(Collection<CacheVideo> cacheVideo) {
		return cacheVideo.stream().mapToInt(CacheVideo::getVideoSize).sum();
	}

	private long maxGain(Collection<CacheVideo> cacheVideo) {
		return cacheVideo.stream().mapToLong(CacheVideo::getTotalGain).sum();
	}

	private Set<Integer> optimize(List<CacheVideo> videos) {

		int capacity = problem.getCacheCapacity();

		Keep keep = new Keep(capacity + 1, videos.size());
		List<long[]> all = new ArrayList<>();
		long[] current = new long[capacity + 1];
		long[] last = null;
		for (int item = videos.size() - 1; item >= 0; item--) {

			last = current;
			all.add(current);
			current = new long[capacity + 1];

			for (int size = 1; size <= capacity; size++) {
				CacheVideo it = videos.get(item);
				if (it.getVideoSize() <= size) {
					long nv = last[size - it.getVideoSize()] + it.getTotalGain();
					if (nv > last[size]) {
						keep.set(item, size, true);
						current[size] = last[size];
					} else {
						keep.set(item, size, false);
						current[size] = last[size];
					}
				} else {
					keep.set(item, size, false);
					current[size] = last[size];
				}
			}
		}

		Set<Integer> usedVideos = new HashSet<>();
		int w = capacity;
		for (int item = 0; item < videos.size(); item++) {
			if (keep.get(item, w)) {
				usedVideos.add(videos.get(item).getVideoId());
				w = w - videos.get(item).getVideoSize();
			}
		}

		int size = videos.stream().filter(v -> usedVideos.contains(v.getVideoId())).mapToInt(CacheVideo::getVideoSize)
				.sum();

		long gain = videos.stream().filter(v -> usedVideos.contains(v.getVideoId())).mapToLong(CacheVideo::getTotalGain)
				.sum();

		// System.out.println(size + " " + gain);
		// System.out.println(all.get(all.size() - 1));

		return usedVideos;
	}

	private static List<Long> init(int values) {
		List<Long> list = new ArrayList<>();
		for (int index = 0; index <= values; index++) {
			list.add(0l);
		}
		return list;
	}

	static class Keep {
		private final int width;
		private final BitSet bitSet;

		public Keep(int width, int height) {
			this.width = width;
			bitSet = new BitSet(width * height);
		}

		public boolean get(int x, int y) {
			return bitSet.get(x * width + y);
		}

		public void set(int x, int y, boolean value) {
			bitSet.set(x * width + y, value);
		}
	}

}
