package eu.k5.hashcode.q2019

import kotlin.test.Test
import kotlin.test.assertEquals

class MatrixTest {

    @Test
    fun refreshZeros() {
        val matrix = Matrix(1)
        matrix.refreshZeros()
        assertEquals(1, matrix.zerosInCol[0])
        assertEquals(1, matrix.zerosInRow[0])
    }

    @Test
    fun subMinRows_singleCell() {
        val matrix = Matrix(1)
        matrix.set(0, 0, 10)
        matrix.subMinRows()
        assertEquals(0, matrix.get(0, 0))
    }

    @Test
    fun subMinRows_minIs0() {
        val matrix = Matrix(2)
        matrix.set(0, 0, 10)
        matrix.subMinRows()
        assertEquals(10, matrix.get(0, 0))
    }

    @Test
    fun subMinCols_singleCell() {
        val matrix = Matrix(1)
        matrix.set(0, 0, 10)
        matrix.subMinCols()
        assertEquals(0, matrix.get(0, 0))
    }

    @Test
    fun subMinCols_minIs0() {
        val matrix = Matrix(2)
        matrix.set(0, 0, 10)
        matrix.subMinCols()
        assertEquals(10, matrix.get(0, 0))
    }

    @Test
    fun getFirstZeroIndexCol() {
        val matrix = Matrix(2)
        matrix.set(0, 0, 10)
        val index = matrix.getFirstZeroIndexCol(0)
        assertEquals(1, index)
    }


    @Test
    fun getFirstZeroIndexCol_noZero() {
        val matrix = Matrix(1)
        matrix.set(0, 0, 10)
        val index = matrix.getFirstZeroIndexCol(0)
        assertEquals(-1, index)
    }

    @Test
    fun calcIndex() {
        val matrix = Matrix(3)
        val index = matrix.calcIndex(0, 2);
        assertEquals(2, matrix.calcRow(index))
        assertEquals(0, matrix.calcCol(index))
    }

    @Test
    fun calcRow() {
        val matrix = Matrix(3)
        val index = matrix.calcIndex(2, 0);
        assertEquals(0, matrix.calcRow(index))
        assertEquals(2, matrix.calcCol(index))
    }

    @Test
    fun calcRow2() {
        val matrix = Matrix(3)
        val index = matrix.calcIndex(1, 1);
        assertEquals(1, matrix.calcRow(index))
        assertEquals(1, matrix.calcCol(index))
    }
    //2105040000


}