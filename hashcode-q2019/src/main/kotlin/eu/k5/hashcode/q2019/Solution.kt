package eu.k5.hashcode.q2019

import java.lang.IllegalArgumentException

class Solution(
        val problem: Problem
) {

    private val slides = ArrayList<SolutionSlide>()

    private val used = HashSet<Int>()

    fun score(): Long {
        if (slides.size <= 1) {
            return 0L
        }
        var score = 0L
        for (index in 1 until slides.size) {
            val slide1 = slides[index - 1]
            val slide2 = slides[index]
            score += Algorithms.score(slide1.tags(problem), slide2.tags(problem))
        }
        return score
    }

    fun addSlide(slide: Problem.Slide) {

        if (used.contains(slide.photo1.photoId)) {
            throw IllegalArgumentException("Already used: " + slide.photo1.photoId)
        }
        if (slide.photo2 == null) {
            used.add(slide.photo1.photoId)
            slides.add(SolutionSlide(slide.photo1.photoId, -1))
            return
        }

        if (used.contains(slide.photo2.photoId)) {
            throw IllegalArgumentException("Already used: " + slide.photo2.photoId)
        }
        used.add(slide.photo1.photoId)
        used.add(slide.photo2.photoId)

        slides.add(SolutionSlide(slide.photo1.photoId, slide.photo2.photoId))
        return

    }

    fun isUsed(photo: Problem.Photo): Boolean {
        return used.contains(photo.photoId)
    }

    fun isUsed(slide: Problem.Slide): Boolean {
        if (used.contains(slide.photo1.photoId)) {
            return true
        }
        return (slide.photo2 != null && used.contains(slide.photo2
                .photoId))
    }

    class SolutionSlide(
            val photo1: Int,
            val photo2: Int
    ) {

        fun tags(problem: Problem): IntArray {
            if (photo2 == -1) {
                return problem.photos[photo1].tags
            } else {
                return Algorithms.join(problem.photos[photo1].tags, problem.photos[photo2].tags)
            }
        }


    }


}