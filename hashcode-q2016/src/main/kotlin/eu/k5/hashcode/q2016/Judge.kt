package eu.k5.hashcode.q2016

import java.util.*

class Judge(val problem: Problem, val solution: Solution) {

    private val droneLocation = ArrayList<Location>()
    private val droneTime = ArrayList<Int>()
    private val droneLoad = ArrayList<Load>()

    private val warehouses = ArrayList<MutableList<StorageChange>>()

    private val orders = ArrayList<MutableList<StorageChange>>()

    init {
        for (drone in 0 until problem.drones) {
            droneLocation.add(problem.warehouses[0].location)
            droneTime.add(-1)

            val load = Load(problem.products.size)
            droneLoad.add(load)
        }

        for (warehouse in 0 until problem.warehouses.size) {
            warehouses.add(ArrayList<StorageChange>())
        }

        for (order in 0 until problem.orders.size) {
            orders.add(ArrayList())
        }
    }

    fun score(): Long {

        for (command in solution.commands) {
            when (command.action) {
                Solution.Action.LOAD -> load(command)
                Solution.Action.DELIVER -> deliver(command)
                Solution.Action.WAIT -> wait(command)
                Solution.Action.UNLOAD -> unload(command)
            }
            command.action
        }

        // Validate warehouse storage
        //
        validateWareshouses()

        return scoreOrders()
    }


    private fun validateWareshouses() {

        for ((warehouseId, change) in warehouses.withIndex()) {
            validateWarehouse(warehouseId, change)
        }
    }

    private fun validateWarehouse(warehouseId: Int, warehouseChange: List<StorageChange>) {
        val changes = warehouseChange.sortedWith(WarehouseStorageChangeComparator())

        val products = ArrayList(problem.warehouses[warehouseId].products)


        // make sure warehouse never drops below zero
        for (change in changes) {
            products[change.product] += change.amount
            if (products[change.product] < 0) {
                throw IllegalArgumentException("""Not enough products ${change.product} at step ${change.time}""")
            }
        }


    }

    private fun scoreOrders(): Long {
        var score = 0L
        for ((orderId, orderChanges) in orders.withIndex()) {
            score += scoreOrder(orderId, orderChanges)
        }
        return score
    }

    private fun scoreOrder(orderId: Int, orderChange: List<StorageChange>): Long {
        val changes = orderChange.sortedBy { it.time }


        val expected = problem.orders[orderId].items
        val delivered = HashSet<Int>()
        for (change in orderChange) {
            if (delivered.contains(change.product)){
                throw IllegalStateException("removing more than existing")
            }
            if (!expected.contains(change.product)){
                throw IllegalStateException("removing more than existing")
            }
            delivered.add(change.product)
        }
        if (delivered.size != expected.size){
            return 0
        }
        val last = changes.last()
        return Math.ceil(((problem.deadline - last.time) / problem.deadline.toDouble()) * 100).toLong()
    }

    private fun deliver(command: Solution.Command) {
        val current = droneLocation[command.drone]
        val order = problem.orders[command.target]
        val distance = order.location.distance(current)

        droneTime[command.drone] = droneTime[command.drone] + distance + 1
        droneLocation[command.drone] = order.location

        val load = droneLoad[command.drone]
        val productWeight = problem.products[command.product]

        load.removeProduct(command.product, command.amount, problem.products[command.product])

        orders[command.target].add(StorageChange(droneTime[command.drone], command.product, command.amount))
    }

    private fun load(command: Solution.Command) {

        val current = droneLocation[command.drone]
        val warehouse = problem.warehouses[command.target]
        val distance = current.distance(warehouse.location)

        droneTime[command.drone] = droneTime[command.drone] + distance + 1
        droneLocation[command.drone] = warehouse.location

        val load = droneLoad[command.drone]

        val productWeight = problem.products[command.product]

        load.addProduct(command.product, command.amount, problem.products[command.product])

        if (problem.capacity < load.totalWeight) {
            throw IllegalStateException("Max load exceeded")
        }

        warehouses[command.target].add(StorageChange(droneTime[command.drone], command.product, -command.amount))
    }


    private fun unload(command: Solution.Command) {
        val current = droneLocation[command.drone]
        val warehouse = problem.warehouses[command.target]
        val distance = current.distance(warehouse.location)

        droneTime[command.drone] = droneTime[command.drone] + distance + 1
        droneLocation[command.drone] = warehouse.location

        val load = droneLoad[command.drone]


        load.removeProduct(command.product, command.amount, problem.products[command.product])

        if (problem.capacity < load.totalWeight) {
            throw IllegalStateException("Max load exceeded")
        }

        warehouses[command.target].add(StorageChange(droneTime[command.drone], command.product, command.amount))
    }

    private fun wait(command: Solution.Command) {
        droneTime[command.drone] += command.amount
    }


    class Load(productCount: Int) {
        var totalWeight = 0
        val products = ArrayList<Int>(productCount)

        init {
            for (p in 0 until productCount) {
                products.add(0)
            }
        }

        fun addProduct(product: Int, amount: Int, weight: Int) {
            totalWeight += amount * weight
            products[product] += amount
        }

        fun removeProduct(product: Int, amount: Int, weight: Int) {

            products[product] -= amount
            if (products[product] < 0) {
                throw IllegalStateException("Drone does not have enough of this product")
            }
            totalWeight -= amount * weight
        }
    }


    class StorageChange(
            val time: Int,
            val product: Int,
            val amount: Int
    )

    class WarehouseStorageChangeComparator : Comparator<StorageChange> {
        override fun compare(o1: StorageChange?, o2: StorageChange?): Int {

            val byTime = Integer.compare(o1!!.time, o2!!.time)
            if (byTime != 0) {
                return byTime
            }
            // Execute positive amounts first
            return Integer.compare(o2!!.amount, o1!!.amount)
        }
    }
}