package eu.k5.hashcode.q2019

class Algorithms {


    companion object {

        fun score(photo1: Problem.Photo, photo2: Problem.Photo): Int {
            return score(photo1.tags, photo2.tags)
        }


        fun combineWaste(photo1: Problem.Slide, photo2: Problem.Slide, avg: Int): Int {
            var common = common(photo1.tags, photo2.tags)
            common += ( Math.abs(photo1.tags.size + photo2.tags.size - 2 * avg) / 2).toInt()
            return common
        }

        fun combineWaste(photo1: Problem.Photo, photo2: Problem.Photo, avg: Int): Int {
            var common = common(photo1.tags, photo2.tags)
            common += Math.abs(photo1.tags.size + photo2.tags.size - 2 * avg)
            return common
        }


        fun join(first: IntArray, second: IntArray): IntArray {
            val join = HashSet<Int>()
            join.addAll(first.toList())
            join.addAll(second.toList())

            val list = ArrayList<Int>(join)
            list.sort()
            return list.toIntArray()
        }

        fun common(tags1: IntArray, tags2: IntArray): Int {
            var index1 = 0;
            var index2 = 0;
            var common: Int = 0;
            while (index1 < tags1.size && index2 < tags2.size) {
                if (tags1[index1] == tags2[index2]) {
                    common++
                    index1++;
                    index2++;
                } else if (tags1[index1] < tags2[index2]) {
                    index1++;
                } else if (tags1[index1] > tags2[index2]) {
                    index2++;
                }
            }
            return common
        }

        fun score(tags1: IntArray, tags2: IntArray): Int {
            val common = common(tags1, tags2)

            val diff = Math.min(tags1.size - common, tags2.size - common)
            return Math.min(diff, common)
        }
    }
}