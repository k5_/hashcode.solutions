package eu.k5.hashcode.f2018;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import eu.k5.hashcode.f2018.Problem.Building;
import eu.k5.hashcode.f2018.Problem.Occupy;

public class ProblemParser {

	private int rows;
	private int cols;
	private int building;
	private int walking;
	private List<Building> residential;

	private Map<Integer, List<Building>> buildings;

	public static Problem parse(BufferedReader reader) throws IOException {
		ProblemParser parser = new ProblemParser();
		parser.setHeader(reader);
		parser.parseBuildings(reader);
		return parser.getProblem();
	}

	private Problem getProblem() {
		return new Problem(rows, cols, building, walking, residential, buildings);
	}

	private void parseBuildings(BufferedReader reader) throws IOException {
		buildings = new HashMap<>(building);
		residential = new ArrayList<>(building);
		for (int index = 0; index < building; index++) {
			Building building = parseBuilding(index, reader);
			if (building.isResidential()) {
				residential.add(building);
			} else {
				buildings.computeIfAbsent(building.getValue(), (__) -> new ArrayList<>()).add(building);
			}
		}
	}

	private Building parseBuilding(int id, BufferedReader reader) throws IOException {
		String buildingHeader = reader.readLine();
		String[] parts = buildingHeader.split(" ");

		boolean residential = "R".equals(parts[0]);
		int height = Integer.parseInt(parts[1]);
		int width = Integer.parseInt(parts[2]);
		int value = Integer.parseInt(parts[3]);

		List<Occupy> occupies = new ArrayList<>();
		for (int h = 0; h < height; h++) {
			String line = reader.readLine();
			for (int w = 0; w < width; w++) {
				if (line.charAt(w) == '#') {
					occupies.add(new Occupy(h, w));
				}
			}
		}

		return new Building(id, residential, height, width, value, occupies);

	}

	private void setHeader(BufferedReader reader) throws IOException {
		String headerLine = reader.readLine();
		String[] parts = headerLine.split(" ");

		rows = Integer.parseInt(parts[0]);
		cols = Integer.parseInt(parts[1]);
		walking = Integer.parseInt(parts[2]);
		building = Integer.parseInt(parts[3]);
	}

}
