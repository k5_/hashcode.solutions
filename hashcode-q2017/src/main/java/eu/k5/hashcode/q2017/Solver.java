package eu.k5.hashcode.q2017;

public interface Solver {

	Solution solve();
}
