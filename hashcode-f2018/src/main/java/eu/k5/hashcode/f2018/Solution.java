package eu.k5.hashcode.f2018;

import java.io.BufferedWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import eu.k5.hashcode.f2018.Problem.Building;

public class Solution {
	private static final char NEW_LINE = '\n';
	private static final char SPACE = ' ';
	private List<BuildingPosition> buildings = new ArrayList<>();

	public void write(BufferedWriter writer) throws IOException {
		writer.write(Integer.toString(buildings.size()));

		writer.write(NEW_LINE);
		for (BuildingPosition building : buildings) {
			writer.write(Integer.toString(building.getBuildingId()));
			writer.write(SPACE);
			writer.write(Integer.toString(building.getRow()));
			writer.write(SPACE);
			writer.write(Integer.toString(building.getColumn()));
			writer.write(NEW_LINE);
		}

	}

	public void addBuilding(int row, int col, int id, Building b, Problem p) {
		if (row + b.getHeight() > p.getRows()) {
			return;
		}
		if (col + b.getWidth() > p.getCols()) {
			return;
		}
		buildings.add(new BuildingPosition(id, col, row));
	}

	static class BuildingPosition {
		private final int buildingId;
		private final int column;
		private final int row;

		public BuildingPosition(int buildingId, int column, int row) {
			this.buildingId = buildingId;
			this.column = column;
			this.row = row;
		}

		public int getBuildingId() {
			return buildingId;
		}

		public int getColumn() {
			return column;
		}

		public int getRow() {
			return row;
		}

	}

}
