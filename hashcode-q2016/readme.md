=== Solver for Google HashCode 2016 Qualification

Scores

busy_day 100520
mother_of_all_warehouses 74317
redundancy 96425
total 271262

(Would have been enough for final round through waitinglist)

== Solver By Distance
This solver does not support multiple warehouses, so it is only used for "mother_of_all_warehouses" testset


* while there are open orders

* foreach drone

  * while drone is not full or more than 5 different orders have been cheched

    * Try to fill current drone with the items from closest open order
      [warehouse location for first order, last order for all others]

  * Add found loads and delivers to solution


== Solver with drone assignment
This solver can switch between multiple warehouses, so it is used for "redundany" and "busy_day"


* while there are open orders
* foreach drone

  * Use closest warehouse that can fullfil orders to current location

  * while drone is not full or more than 5 different orders have been cheched

    * Try to fill current drone with the items from closest open order
      [warehouse location for first order, last order for all others]

  * If items where to load, check again all orders by distance to current warehouse
    * If no items where found to load, mark warehouse as empty

  * Add found loads and delivers to solution

